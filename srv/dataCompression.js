var fs = require('fs');

var data = JSON.parse(fs.readFileSync('../data/json/event000000000-hits.json', 'utf8'));

data = new Float32Array([].concat(...data.Hits.Spacepoints));

var wstream = fs.createWriteStream('../data/json/event000000000-hits.dat');
var buffer = new Buffer(data.length * 4);
	for(var i = 0; i < data.length; i++){
		//write the float in Little-Endian and move the offset
		buffer.writeFloatLE(data[i], i * 4);
	}
wstream.write(buffer);
wstream.end();