var fs = require('fs')

var data;

fs.readFile('../data/json/event000000000-hits.dat', (err, buf) => {
	data = new Float32Array(buf.buffer, buf.offset, buf.byteLength/4);

	for (var i = 1; i <= 20; i++) {
		var randomisedData = [];
		data.forEach( (elt, idx) => {randomisedData[idx] = elt * ((2 * Math.random() - 1) / 100 + 1)} );

		fs.writeFileSync('../data/json/event00000000' + i + '-hits.dat', new Buffer(new Float32Array(randomisedData).buffer));
	}
});