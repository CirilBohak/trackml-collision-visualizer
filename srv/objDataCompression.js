var fs = require('fs');

var verts = [];
fs.readFile('../data/obj/LStrip.obj', 'utf8', function(err, data) {
	if (err) throw err;
	data = data.split("\r\n");
	for (var i = 0; i < data.length; i++) {
		var s = data[i].split(" ");
		if (s[0] !== "f")
			verts.push(s.splice(1, 3));
	}
	verts = [].concat(...verts);
	verts.forEach( function (elt, idx) {
		verts[idx] = parseFloat(elt);
	});
	verts = new Float32Array(verts);

	var wstream = fs.createWriteStream('../data/obj/LStrip-min.dat');
	var buffer = new Buffer(verts.length * 4);
		for(var i = 0; i < verts.length; i++){
			//write the float in Little-Endian and move the offset
			buffer.writeFloatLE(verts[i], i * 4);
		}
	wstream.write(buffer);
	wstream.end();
});


/*var requestLStrip = new XMLHttpRequest();
requestLStrip.open('GET', './data/obj/LStrip-min.dat', true);
requestLStrip.responseType = 'arraybuffer';
requestLStrip.onload = function(msg) {
	var geometry = new THREE.BufferGeometry();
	var indices = [];
	var vertices = new Float32Array(this.response);
	for (var i=0; i < vertices.length / 12; i+=4) {
		indices.push( i+1, i+2, i+3 );
		indices.push( i+1, i+3, i+4 );
	}
	geometry.setIndex( indices );
	geometry.addAttribute( 'position', new THREE.Float32BufferAttribute( vertices, 3 ) );
	mesh = new THREE.Mesh( geometry, materialLStrip );
	scene.add( mesh );
};
requestLStrip.send();*/