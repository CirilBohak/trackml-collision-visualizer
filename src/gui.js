var params = {
    visibleGeometry: true,
    MSAA: false,

    XYcutPlaneOffset: 0.0,
    ZcutPlaneOffset: 0.0,

    visibleBeamPipe: true,
    colorBeamPipe: 0x282828,
    ecolorBeamPipe: 0x50505,
    opacityBeamPipe: 0.16,

    visiblePix: true,
    //colorPix: 0x119911,
    colorPix: 0xb3f1ff,
    ecolorPix: 0x1eff,
    //opacityPix: 0.15,
    opacityPix: 0.04,

    visiblePST: true,
    //colorPST: 0x555555,
    colorPST: 0x464646,
    ecolorPST: 0x0,
    opacityPST: 0.13,

    visibleSStrip: true,
    //colorSStrip: 0x111199,
    colorSStrip: 0x1424a,
    ecolorSStrip: 0x2a2a2a,
    opacitySStrip: 0.08,

    visibleLStrip: true,
    colorLStrip: 0x8e8d8d,
    ecolorLStrip: 0x343434,
    opacityLStrip: 0.07,

    lightIntensity: 1.0,

    //visibleHits: true,
    //hitOpacity: 0.5,
    //hitDefaultColor: 0xffffff,
    //hitPointSize: 2.0,
    //hitProperty: false,
    ///eventID: 0,

    //visibleTracks: true,

    //test: 1.0,


    visibleParticles: true,
    particlesSize: 3.0,
    particlesColor: 0xffffff,
    opacityParticles: 1.0,
    particlesColorType: null,

    visibleHits: true,
    hitsSize: 1.3,
    hitsColor: 0x996699,
    opacityHits: 0.5,
    hitsColorType: 0,

    visibleTracks: true,
    tracksSize: 1.0,
    tracksColor: 0xff0000,
    submittedTracksColor: 0xf0ffff,
    opacityTracks: 0.04,
    tracksColorType: null,

    visibleSelectedTrack: true,
    selectedTrackID: -1,
    widthSelectedTrack: 4.0,
    opacitySelectedTrack: 1.0,
    selectedTrackColor: 0xffffff,
    selectedSubmissionTrackColor: 0xffa500,
    selectedTrackColorType: null,

    visibleSelectedHit: true,
    selectedHitID: -1,
    sizeSelectedHit: 16.0,
    opacitySelectedHit: 1.0,
    selectedHitColor: 0xffffff,
    selectedHitColorType: 1,

    searchEventID: "",
    searchHitID: "",
    searchTrackID: "",

    //compareSubmissionData: false,


    LStripBRenderOrder: 1,
    SStripBRenderOrder: 2,
    PSTBRenderOrder: 3,
    pixBRenderOrder: 4,
    beamPipeBRenderOrder: 5,

    beamPipeFRenderOrder: 6 +102,
    pixFRenderOrder: 7 +102,
    PSTFRenderOrder: 8 +102,
    SStripFRenderOrder: 9 +102,
    LStripFRenderOrder: 10 +102,

    particlesRenderOrder: 100,
    hitsRenderOrder: 101,
    tracksRenderOrder: 102,
    submittedTracksRenderOrder: 102,
    selectedTrackRenderOrder: 102,


    colorOK: "#125634",
    //colorErr: "#563412",
    colorErr: "#561212",
    colorWarn: "#535312"
};


let startEventID = 1000;
let endEventID = 1099; endEventID = 1000-1;
let EventIDMaxLength = 9;


var guiTracks, guiSubmissionTracks;

var eventList = [], searchEventList = [];
var guiEventListSection = 0, guiSearchEventListSection = guiEventListSection;
var guiEventsPerSection = 8, guiSearchEventsPerSection = guiEventsPerSection;
var eventLoaded = false;
var selectedEventID;

var folderTracks, folderSubmissionTracks;
var trackList = {}, submissionTrackList = {}, searchTrackList = {};
var guiTrackListSection = 0, guiSearchTrackListSection = guiTrackListSection;
var guiTracksPerSection = 8, guiSearchTracksPerSection = guiTracksPerSection;
var submissionTrackGUInitialized= false;
let g_guiTrackControllerList = [];

var folderHits;
var hitList = [], searchHitList = [];
var guiHitListSection = 0, guiSearchHitListSection = guiHitListSection;
var guiHitsPerSection = 8, guiSearchHitsPerSection = guiHitsPerSection;




function initGUI(){
    initParametersGUI();
    initEventsGUI();
}
function initEventsGUI(){
    readEventsInfo(startEventID, endEventID, EventIDMaxLength);
    addEventsGUI();
}
function initParametersGUI() {
    let gui = new dat.GUI();


    //geometry
    var folderGeometry = gui.addFolder("Geometry");
    folderGeometry.add(params, "visibleGeometry").onChange( () => {
        params.visibleBeamPipe = params.visibleGeometry;
        params.visiblePix      = params.visibleGeometry;
        params.visiblePST      = params.visibleGeometry;
        params.visibleSStrip   = params.visibleGeometry;
        params.visibleLStrip   = params.visibleGeometry;

        //combined geometry
        updateCombinedDetectorGeometry();

        folderBeamPipe.__controllers[0].setValue(params.visibleGeometry);
        folderPix.__controllers[0].setValue(params.visibleGeometry);
        folderPST.__controllers[0].setValue(params.visibleGeometry);
        folderSStrip.__controllers[0].setValue(params.visibleGeometry);
        folderLStrip.__controllers[0].setValue(params.visibleGeometry);
    });
    /*folderGeometry.add(params, "MSAA").onChange( function () {
        renderer.antialias = params.MSAA;
    } );*/
    folderGeometry.add(params, "XYcutPlaneOffset", -1200, 1200).onChange( function () {
        planeX.constant = -params.XYcutPlaneOffset;
        planeY.constant = -params.XYcutPlaneOffset;
    } );
    folderGeometry.add(params, "ZcutPlaneOffset", -3000, 3000).onChange( function () {
        planeZ.constant = -params.ZcutPlaneOffset;
    } );

    //BeamPipe
    var folderBeamPipe = folderGeometry.addFolder("BeamPipe");
    folderBeamPipe.add(params, "visibleBeamPipe").onChange( () => {
        BeamPipe.visible = params.visibleBeamPipe;
        BeamPipeB.visible = params.visibleBeamPipe;
    });
    folderBeamPipe.add(params, "opacityBeamPipe", 0, 1).onChange( function () {
        materialBeamPipe.opacity = params.opacityBeamPipe;
        materialBeamPipeB.opacity = params.opacityBeamPipe;
    } );
    folderBeamPipe.addColor(params, "colorBeamPipe").onChange( function () {
        materialBeamPipe.color.setHex(params.colorBeamPipe);
        materialBeamPipeB.color.setHex(params.colorBeamPipe);

        if(hitMaterial !== undefined) hitMaterial.uniforms.colorBeamPipe.value = new THREE.Color(params.colorBeamPipe).add(new THREE.Color(params.ecolorBeamPipe));
        if(trackMaterial !== undefined) trackMaterial.uniforms.colorBeamPipe.value = new THREE.Color(params.colorBeamPipe).add(new THREE.Color(params.ecolorBeamPipe));
        if(selectedTrackMaterial !== undefined) selectedTrackMaterial.uniforms.colorBeamPipe.value = new THREE.Color(params.colorBeamPipe).add(new THREE.Color(params.ecolorBeamPipe));
    } );
    folderBeamPipe.addColor(params, "ecolorBeamPipe").onChange( function () {
        materialBeamPipe.emissive.setHex(params.ecolorBeamPipe);
        materialBeamPipeB.emissive.setHex(params.ecolorBeamPipe);

        if(hitMaterial !== undefined) hitMaterial.uniforms.colorBeamPipe.value = new THREE.Color(params.colorBeamPipe).add(new THREE.Color(params.ecolorBeamPipe));
        if(trackMaterial !== undefined) trackMaterial.uniforms.colorBeamPipe.value = new THREE.Color(params.colorBeamPipe).add(new THREE.Color(params.ecolorBeamPipe));
        if(selectedTrackMaterial !== undefined) selectedTrackMaterial.uniforms.colorBeamPipe.value = new THREE.Color(params.colorBeamPipe).add(new THREE.Color(params.ecolorBeamPipe));
    } );

    //Pix
    var folderPix = folderGeometry.addFolder("Pix");
    folderPix.add(params, "visiblePix").onChange( () => {
        Pix.visible = params.visiblePix;
        PixB.visible = params.visiblePix;

        updateCombinedDetectorGeometry();
    });
    folderPix.add(params, "opacityPix", 0, 1).onChange( function () {
        materialPix.opacity = params.opacityPix;
        materialPixB.opacity = params.opacityPix;


        if(combinedDetectorsMaterial !== undefined) combinedDetectorsMaterial.uniforms.detector_type_alphas.value[0] = params.opacityPix;
    } );
    folderPix.addColor(params, "colorPix").onChange( function () {
        materialPix.color.setHex(params.colorPix);
        materialPixB.color.setHex(params.colorPix);

        if(hitMaterial !== undefined) hitMaterial.uniforms.colorPix.value = new THREE.Color(params.colorPix).add(new THREE.Color(params.ecolorPix));
        if(trackMaterial !== undefined) trackMaterial.uniforms.colorPix.value = new THREE.Color(params.colorPix).add(new THREE.Color(params.ecolorPix));
        if(selectedTrackMaterial !== undefined) selectedTrackMaterial.uniforms.colorPix.value = new THREE.Color(params.colorPix).add(new THREE.Color(params.ecolorPix));


        if(combinedDetectorsMaterial !== undefined) combinedDetectorsMaterial.uniforms.detector_type_colors.value[0] = new THREE.Color(params.colorPix);
    } );
    folderPix.addColor(params, "ecolorPix").onChange( function () {
        materialPix.emissive.setHex(params.ecolorPix);
        materialPixB.emissive.setHex(params.ecolorPix);

        if(hitMaterial !== undefined) hitMaterial.uniforms.colorPix.value = new THREE.Color(params.colorPix).add(new THREE.Color(params.ecolorPix));
        if(trackMaterial !== undefined) trackMaterial.uniforms.colorPix.value = new THREE.Color(params.colorPix).add(new THREE.Color(params.ecolorPix));
        if(selectedTrackMaterial !== undefined) selectedTrackMaterial.uniforms.colorPix.value = new THREE.Color(params.colorPix).add(new THREE.Color(params.ecolorPix));


        if(combinedDetectorsMaterial !== undefined) combinedDetectorsMaterial.uniforms.detector_type_ecolors.value[0] = new THREE.Color(params.ecolorPix);
    } );

    //PST
    var folderPST = folderGeometry.addFolder("PST");
    folderPST.add(params, "visiblePST").onChange( () => {
        PST.visible = params.visiblePST;
        PSTB.visible = params.visiblePST;
    });
    folderPST.add(params, "opacityPST", 0, 1).onChange( function () {
        materialPST.opacity = params.opacityPST;
        materialPSTB.opacity = params.opacityPST;
    } );
    folderPST.addColor(params, "colorPST").onChange( function () {
        materialPST.color.setHex(params.colorPST);
        materialPSTB.color.setHex(params.colorPST);

        if(hitMaterial !== undefined) hitMaterial.uniforms.colorPST.value = new THREE.Color(params.colorPST).add(new THREE.Color(params.ecolorPST));
        if(trackMaterial !== undefined) trackMaterial.uniforms.colorPST.value = new THREE.Color(params.colorPST).add(new THREE.Color(params.ecolorPST));
        if(selectedTrackMaterial !== undefined) selectedTrackMaterial.uniforms.colorPST.value = new THREE.Color(params.colorPST).add(new THREE.Color(params.ecolorPST));
    } );
    folderPST.addColor(params, "ecolorPST").onChange( function () {
        materialPST.emissive.setHex(params.ecolorPST);
        materialPSTB.emissive.setHex(params.ecolorPST);

        if(hitMaterial !== undefined) hitMaterial.uniforms.colorPST.value = new THREE.Color(params.colorPST).add(new THREE.Color(params.ecolorPST));
        if(trackMaterial !== undefined) trackMaterial.uniforms.colorPST.value = new THREE.Color(params.colorPST).add(new THREE.Color(params.ecolorPST));
        if(selectedTrackMaterial !== undefined) selectedTrackMaterial.uniforms.colorPST.value = new THREE.Color(params.colorPST).add(new THREE.Color(params.ecolorPST));
    } );

    //SStrip
    var folderSStrip = folderGeometry.addFolder("SStrip");
    folderSStrip.add(params, "visibleSStrip").onChange( () => {
        SStrip.visible = params.visibleSStrip;
        SStripB.visible = params.visibleSStrip;

        updateCombinedDetectorGeometry();
    });
    folderSStrip.add(params, "opacitySStrip", 0, 1).onChange( function () {
        materialSStrip.opacity = params.opacitySStrip;
        materialSStripB.opacity = params.opacitySStrip;


        if(combinedDetectorsMaterial !== undefined) combinedDetectorsMaterial.uniforms.detector_type_alphas.value[1] = params.opacitySStrip;
    } );
    folderSStrip.addColor(params, "colorSStrip").onChange( function () {
        materialSStrip.color.setHex(params.colorSStrip);
        materialSStripB.color.setHex(params.colorSStrip);


        if(combinedDetectorsMaterial !== undefined) combinedDetectorsMaterial.uniforms.detector_type_colors.value[1] = new THREE.Color(params.colorSStrip);


        if(hitMaterial !== undefined) hitMaterial.uniforms.colorSStrip.value = new THREE.Color(params.colorSStrip).add(new THREE.Color(params.ecolorSStrip));
        if(trackMaterial !== undefined) trackMaterial.uniforms.colorSStrip.value = new THREE.Color(params.colorSStrip).add(new THREE.Color(params.ecolorSStrip));
        if(selectedTrackMaterial !== undefined) selectedTrackMaterial.uniforms.colorSStrip.value = new THREE.Color(params.colorSStrip).add(new THREE.Color(params.ecolorSStrip));
    } );
    folderSStrip.addColor(params, "ecolorSStrip").onChange( function () {
        materialSStrip.emissive.setHex(params.ecolorSStrip);
        materialSStripB.emissive.setHex(params.ecolorSStrip);


        if(combinedDetectorsMaterial !== undefined) combinedDetectorsMaterial.uniforms.detector_type_ecolors.value[1] = new THREE.Color(params.ecolorSStrip);


        if(hitMaterial !== undefined) hitMaterial.uniforms.colorSStrip.value = new THREE.Color(params.colorSStrip).add(new THREE.Color(params.ecolorSStrip));
        if(trackMaterial !== undefined) trackMaterial.uniforms.colorSStrip.value = new THREE.Color(params.colorSStrip).add(new THREE.Color(params.ecolorSStrip));
        if(selectedTrackMaterial !== undefined) selectedTrackMaterial.uniforms.colorSStrip.value = new THREE.Color(params.colorSStrip).add(new THREE.Color(params.ecolorSStrip));
    } );

    //LStrip
    var folderLStrip = folderGeometry.addFolder("LStrip");
    folderLStrip.add(params, "visibleLStrip").onChange( () => {
        LStrip.visible = params.visibleLStrip;
        LStripB.visible = params.visibleLStrip;

        updateCombinedDetectorGeometry();
    });
    folderLStrip.add(params, "opacityLStrip", 0, 1).onChange( function () {
        materialLStrip.opacity = params.opacityLStrip;
        materialLStripB.opacity = params.opacityLStrip;


        if(combinedDetectorsMaterial !== undefined) combinedDetectorsMaterial.uniforms.detector_type_alphas.value[2] = params.opacityLStrip;
    } );
    folderLStrip.addColor(params, "colorLStrip").onChange( function () {
        materialLStrip.color.setHex(params.colorLStrip);
        materialLStripB.color.setHex(params.colorLStrip);


        if(combinedDetectorsMaterial !== undefined) combinedDetectorsMaterial.uniforms.detector_type_colors.value[2] = new THREE.Color(params.colorLStrip);


        if(hitMaterial !== undefined) hitMaterial.uniforms.colorLStrip.value = new THREE.Color(params.colorLStrip).add(new THREE.Color(params.ecolorLStrip));
        if(trackMaterial !== undefined) trackMaterial.uniforms.colorLStrip.value = new THREE.Color(params.colorLStrip).add(new THREE.Color(params.ecolorLStrip));
        if(selectedTrackMaterial !== undefined) selectedTrackMaterial.uniforms.colorLStrip.value = new THREE.Color(params.colorLStrip).add(new THREE.Color(params.ecolorLStrip));
    } );
    folderLStrip.addColor(params, "ecolorLStrip").onChange( function () {
        materialLStrip.emissive.setHex(params.ecolorLStrip);
        materialLStripB.emissive.setHex(params.ecolorLStrip);


        if(combinedDetectorsMaterial !== undefined) combinedDetectorsMaterial.uniforms.detector_type_ecolors.value[2] = new THREE.Color(params.ecolorLStrip);


        if(hitMaterial !== undefined) hitMaterial.uniforms.colorLStrip.value = new THREE.Color(params.colorLStrip).add(new THREE.Color(params.ecolorLStrip));
        if(trackMaterial !== undefined) trackMaterial.uniforms.colorLStrip.value = new THREE.Color(params.colorLStrip).add(new THREE.Color(params.ecolorLStrip));
        if(selectedTrackMaterial !== undefined) selectedTrackMaterial.uniforms.colorLStrip.value = new THREE.Color(params.colorLStrip).add(new THREE.Color(params.ecolorLStrip));
    } );


    //properties
    var folderProperties = gui.addFolder("Properties");

    //particles
    var folderParticles = folderProperties.addFolder("particles");
    folderParticles.add(params, "visibleParticles").onChange( () => {
        if(particlesObject !== undefined) particlesObject.visible = params.visibleParticles;
    });
    folderParticles.add(params, "particlesSize", 0).onChange( () => {
        if(particleMaterial !== undefined) particleMaterial.uniforms.particle_size.value = params.particlesSize;
    });
    folderParticles.add(params, "opacityParticles", 0, 1).onChange( function () {
        if(particleMaterial !== undefined) particleMaterial.uniforms.particle_alpha.value = params.opacityParticles;
    } );
    folderParticles.addColor(params, 'particlesColor').onChange( function () {
        if(particleMaterial !== undefined) {
            var col = new THREE.Color(params.particlesColor);
            particleMaterial.uniforms.particle_color.value.x = col.r;
            particleMaterial.uniforms.particle_color.value.y = col.g;
            particleMaterial.uniforms.particle_color.value.z = col.b;
            //particleMaterial.needsUpdate = true;
        }
    } );
    var PCTController = folderParticles.add(params, "particlesColorType", {attribute: 0, uniform: 1});
    PCTController.onChange(function(){
        if(particleMaterial !== undefined) particleMaterial.uniforms.particle_color_type.value = params.particlesColorType;
    });
    PCTController.setValue(1);

    //hits
    var folderHits = folderProperties.addFolder("hits");
    folderHits.add(params, "visibleHits").onChange( () => {
        if(hitsObject !== undefined) hitsObject.visible = params.visibleHits;
    });
    folderHits.add(params, "hitsSize", 0).onChange( () => {
        if(hitMaterial !== undefined) hitMaterial.uniforms.hit_size.value = params.hitsSize;
    });
    folderHits.add( params, "opacityHits", 0, 1 ).onChange( function () {
        if(hitMaterial !== undefined) hitMaterial.uniforms.hit_alpha.value = params.opacityHits;
    } );
    folderHits.addColor( params, 'hitsColor').onChange( function () {
        if(hitMaterial !== undefined) {
            var col = new THREE.Color(params.hitsColor);
            hitMaterial.uniforms.hit_color.value.x = col.r;
            hitMaterial.uniforms.hit_color.value.y = col.g;
            hitMaterial.uniforms.hit_color.value.z = col.b;
            //hitMaterial.needsUpdate = true;
        }
    } );
    var HCTController = folderHits.add(params, "hitsColorType", {geometry: 0, uniform: 1}).onChange(function(){
        if(hitMaterial !== undefined) hitMaterial.uniforms.hit_color_type.value = params.hitsColorType;
    });
    //folderHits.__controllers[4].setValue(0);
    HCTController.setValue(0);

    //tracks
    var folderTracks = folderProperties.addFolder("tracks");
    folderTracks.add(params, "visibleTracks").onChange( () => {
        if(tracksObject !== undefined) tracksObject.visible = params.visibleTracks;
        if(submittedTracksObject !== undefined) submittedTracksObject.visible = params.visibleTracks;
    });
    folderTracks.add(params, "opacityTracks", 0, 1).onChange( function () {
        if(trackMaterial !== undefined) trackMaterial.uniforms.track_alpha.value = params.opacityTracks;
        if(submittedTrackMaterial !== undefined) submittedTrackMaterial.uniforms.track_alpha.value = params.opacityTracks;
    } );
    folderTracks.addColor(params, 'tracksColor').onChange( function () {
        if(trackMaterial !== undefined) {
            var col = new THREE.Color(params.tracksColor);
            trackMaterial.uniforms.track_color.value.x = col.r;
            trackMaterial.uniforms.track_color.value.y = col.g;
            trackMaterial.uniforms.track_color.value.z = col.b;
            //trackMaterial.needsUpdate = true;
        }
    } );
    var TCTController = folderTracks.add(params, "tracksColorType", {attribute: 0, uniform: 1, geometry: 2});
    TCTController.onChange(function(){
        if(trackMaterial !== undefined) trackMaterial.uniforms.track_color_type.value = params.tracksColorType;
        if(submittedTrackMaterial !== undefined) submittedTrackMaterial.uniforms.track_color_type.value = params.tracksColorType;
    });
    TCTController.setValue(1);

    //selected track
    var folderSelectedTrack = folderProperties.addFolder("selected track");
    folderSelectedTrack.add(params, "visibleSelectedTrack").onChange( () => {
        if(selectedTrackObject !== undefined) selectedTrackObject.visible = params.visibleSelectedTrack;
        if(selectedSubmissionTrackObject !== undefined) selectedSubmissionTrackObject.visible = params.visibleSelectedTrack;

        if(particleMaterial !== undefined) particleMaterial.uniforms.selectedTrackVisible.value = +params.visibleSelectedTrack; //+boolean => 0/1
        if(hitMaterial !== undefined) hitMaterial.uniforms.selectedTrackVisible.value = +params.visibleSelectedTrack;
        if(trackMaterial !== undefined) trackMaterial.uniforms.selectedTrackVisible.value = +params.visibleSelectedTrack;
    });
    folderSelectedTrack.add(params, "widthSelectedTrack", 0).onChange( () => {
        if(selectedTrackMaterial !== undefined) selectedTrackMaterial.uniforms.line_width.value = params.widthSelectedTrack;
        if(selectedSubmissionTrackMaterial !== undefined) selectedSubmissionTrackMaterial.uniforms.line_width.value = params.widthSelectedTrack;
    });
    folderSelectedTrack.add(params, "opacitySelectedTrack", 0, 1).onChange( function () {
        if(selectedTrackMaterial !== undefined) selectedTrackMaterial.uniforms.track_alpha.value = params.opacitySelectedTrack;
        if(selectedSubmissionTrackMaterial !== undefined) selectedSubmissionTrackMaterial.uniforms.track_alpha.value = params.opacitySelectedTrack;
    } );
    folderSelectedTrack.addColor(params, "selectedTrackColor").onChange( function () {
        if(selectedTrackMaterial !== undefined) {
            var col = new THREE.Color(params.selectedTrackColor);
            selectedTrackMaterial.uniforms.track_color.value.x = col.r;
            selectedTrackMaterial.uniforms.track_color.value.y = col.g;
            selectedTrackMaterial.uniforms.track_color.value.z = col.b;
            //trackMaterial.needsUpdate = true;
        }

        //change color of already clicked/selected items
        var array = document.getElementsByClassName("trackColorBar");
        for(var i = 0; i < array.length; i++){
            array[i].style.backgroundColor = '#' + ("000000" + params.selectedTrackColor.toString(16)).substr(-6);
        }
    } );
    var STCTController = folderSelectedTrack.add(params, "selectedTrackColorType", {attribute: 0, uniform: 1, geometry: 2});
    STCTController.onChange(function(){
        if(selectedTrackMaterial !== undefined) selectedTrackMaterial.uniforms.track_color_type.value = params.selectedTrackColorType;
        if(selectedSubmissionTrackMaterial !== undefined) selectedSubmissionTrackMaterial.uniforms.track_color_type.value = params.selectedTrackColorType;
    });
    STCTController.setValue(1);

    //selected hit
    var folderSelectedHit = folderProperties.addFolder("selected hit");
    folderSelectedHit.add(params, "visibleSelectedHit").onChange( () => {
        if(hitMaterial !== undefined) hitMaterial.uniforms.selectedHitVisible.value = +params.visibleSelectedHit;
    });
    folderSelectedHit.add(params, "sizeSelectedHit", 0).onChange( () => {
        if(hitMaterial !== undefined) hitMaterial.uniforms.selected_hit_size.value = params.sizeSelectedHit;
    });
    folderSelectedHit.add(params, "opacitySelectedHit", 0, 1).onChange( function () {
        if(hitMaterial !== undefined) hitMaterial.uniforms.selected_hit_alpha.value = params.opacitySelectedHit;
    } );
    folderSelectedHit.addColor(params, "selectedHitColor").onChange( function () {
        if(hitMaterial !== undefined) {
            var col = new THREE.Color(params.selectedHitColor);
            hitMaterial.uniforms.selected_hit_color.value.x = col.r;
            hitMaterial.uniforms.selected_hit_color.value.y = col.g;
            hitMaterial.uniforms.selected_hit_color.value.z = col.b;
            //trackMaterial.needsUpdate = true;

            //change color of clicked/selected item
            //1. clear previously colored background
            var array = document.getElementsByClassName("hitColorBar");
            for(var i = 0; i < array.length; i++){
                //array[i].style.backgroundColor = '#' + params.selectedHitColor.toString(16);
                array[i].style.backgroundColor = '#' + ("000000" + params.selectedHitColor.toString(16)).substr(-6);
            }
        }
    } );
    var SHCTController = folderSelectedHit.add(params, "selectedHitColorType", {geometry: 0, uniform: 1});
    SHCTController.onChange(function(){
        if(hitMaterial !== undefined) hitMaterial.uniforms.selected_hit_color_type.value = params.selectedHitColorType;
    });
    SHCTController.setValue(1);


    gui.open();
    folderProperties.open();
}
function updateCombinedDetectorGeometry(){
    if(combinedDetectorsObject !== undefined) {
        if(!params.visiblePix && !params.visibleSStrip && !params.visibleLStrip){
            combinedDetectorsObject.geometry.setDrawRange(0, 0);
        }else if(params.visiblePix && !params.visibleSStrip && !params.visibleLStrip){
            combinedDetectorsObject.geometry.setDrawRange(0, Pix.geometry.index.array.length);
        }else if(!params.visiblePix && params.visibleSStrip && !params.visibleLStrip){
            combinedDetectorsObject.geometry.setDrawRange(Pix.geometry.index.array.length, SStrip.geometry.index.array.length);
        }else if(!params.visiblePix && !params.visibleSStrip && params.visibleLStrip){
            combinedDetectorsObject.geometry.setDrawRange(Pix.geometry.index.array.length+SStrip.geometry.index.array.length, LStrip.geometry.index.array.length);
        }else if(params.visiblePix && params.visibleSStrip && !params.visibleLStrip){
            combinedDetectorsObject.geometry.setDrawRange(0, Pix.geometry.index.array.length+SStrip.geometry.index.array.length);
        }else if(!params.visiblePix && params.visibleSStrip && params.visibleLStrip){
            combinedDetectorsObject.geometry.setDrawRange(Pix.geometry.index.array.length, SStrip.geometry.index.array.length+LStrip.geometry.index.array.length);
        }else if(params.visiblePix && !params.visibleSStrip && params.visibleLStrip){
            combinedDetectorsObject.geometry.setDrawRange(Pix.geometry.index.array.length+SStrip.geometry.index.array.length, LStrip.geometry.index.array.length+Pix.geometry.index.array.length);
        }else if(params.visiblePix && params.visibleSStrip && params.visibleLStrip){
            combinedDetectorsObject.geometry.setDrawRange(0, Pix.geometry.index.array.length+SStrip.geometry.index.array.length+LStrip.geometry.index.array.length);
        }

        //combinedDetectorsObject.visible = params.visibleGeometry;
    }
}


function readEventsInfo(startID, endID, EventIDMaxLength){
    //get all event IDs
    //eventList = [{id: "000000001", source: "server"}, {id: "000000016", source: "server"}];  //additional special events
    eventList = [{id: "000001000", source: "server"}];  //additional special events

    //populate event list
    let numberID;
    for(let i = startID; i <= endID; i++){
        numberID = i.toString();
        while(numberID.length < EventIDMaxLength) numberID = "0" + numberID;

        eventList.push({id: numberID, source: "server"});
    }

}

var g_CompareButtonController;
var compareFolderIconHolder;
let g_compareEventLI;
function addEventsGUI(){
    //Event list selection
    //setup gui with found event IDs
    var guiEvents = new dat.GUI();
    guiEvents.domElement.id = "GUIEvents";


    var folderEvents = guiEvents.addFolder("Events");
    var guiEventControllerList = [];


    var NavButton = {
        //folderEvents: folderEvents,
        //controllers: controllers,
        clickPrevButton: function(){
            //console.log("clicked prev");
            if(guiEventListSection > 0) {
                guiEventListSection--;
                //updateGUIEventList(this.folderEvents, this.controllers);
            }
            updateGUIEventList(folderEvents, guiEventControllerList);
        },
        clickNextButton: function(){
            //console.log("clicked next");
            if(guiEventListSection < eventList.length/guiEventsPerSection - 1) {
                guiEventListSection++;
                //updateGUIEventList(this.folderEvents, this.controllers);
            }
            updateGUIEventList(folderEvents, guiEventControllerList);
        }
    };


    var cont1 = folderEvents.add(NavButton, "clickPrevButton").name("Prev");
    var cont2 = folderEvents.add(NavButton, "clickNextButton").name("Next");
    var cont3 = folderEvents.add(params, "searchEventID");


    //set css IDs
    folderEvents.domElement.id = "events";
    /*var li = document.getElementById("events").childNodes[0].childNodes[1];
    li.id = "eventListNav";
    li = document.getElementById("events").childNodes[0].childNodes[2];
    li.id = "eventListNav";*/
    cont1.domElement.parentNode.parentNode.id = "eventListNav";
    cont2.domElement.parentNode.parentNode.id = "eventListNav";
    cont3.domElement.parentNode.id = "eventListSearch";


    //search option
    cont3.onChange(function(value){
        //single match result
        /*var found = eventList.find(function(element){ //undefined ce ne najde nic
            return element.id === value;
        });

        if(found !== undefined){
            guiSearchEventListSection = 0;
            //clear current list
            searchEventList = [];
            //add to list
            searchEventList = [found];
            updateGUISearchEventList(folderEvents, guiEventControllerList);
        }else{
            updateGUIEventList(folderEvents, guiEventControllerList);
        }
        */


        //multiple match
        var found = eventList.filter(function(element){ //empty array ce ne najde nic
            return element.id.includes(value);
        });

        searchEventList = [];
        if(found.length > 0){
            guiSearchEventListSection = 0;
            //clear current list
            ///searchEventList = [];
            //add to list
            searchEventList = found;
            updateGUISearchEventList(folderEvents, guiEventControllerList);
        }else{
            //if no match
            //updateGUIEventList(folderEvents, guiEventControllerList);
            updateGUISearchEventList(folderEvents, guiEventControllerList);
        }
    });


    folderEvents.open();
    updateGUIEventList(folderEvents, guiEventControllerList);


    //COMPARE EVENT SECTION
    //compare button
    var compareButton = {
        compareSubmissionData: function(){
            if(eventLoaded) {
                //run file chooser
                document.getElementById('file_chooser_compare_event').click();


                ////initiate reading of submission file
                //readSubmissionFile("./data/csv/sample_submission.csv", selectedEventID);
            }
        }
    };

    //add submission compare button
    var compareButtonController = guiEvents.add(compareButton, "compareSubmissionData").name("Choose an event from list");
    g_CompareButtonController = compareButtonController;
    compareButtonController.domElement.parentNode.id = "compareSubmissionData";


    let compareEventLI = compareButtonController.domElement.parentNode.parentNode;
    g_compareEventLI = compareEventLI;


    //add event listener to file chooser
    document.getElementById("file_chooser_compare_event").addEventListener("change", function(){
        //loading progress
        //clear previous load progress indicator
        /*if(compareProgressHolder !== undefined){
            compareProgressHolder.parentElement.appendChild(compareProgressHolder.childNodes[0]);
            compareProgressHolder.remove();
            compareProgressHolder = undefined; //clear self variable
        }*/

        //clear folder icon holder (prepare space for loading circle)
        /*if(compareFolderIconHolder !== undefined){
            compareFolderIconHolder.parentElement.appendChild(compareFolderIconHolder.childNodes[0]);
            compareFolderIconHolder.remove();
            compareFolderIconHolder = undefined; //clear self variable
        }*/


        //append new loading progress indicator
        /*var li = compareButtonController.domElement.parentNode.parentNode;

        compareProgressHolder = document.createElement("DIV");
        compareProgressBar = document.createElement("DIV");
        compareProgressCircle = document.createElement("DIV");

        compareProgressCircle.classList.add("loaderCircle");
        compareProgressBar.classList.add("loaderBar");
        compareProgressHolder.classList.add("loaderHolder");

        compareProgressHolder.appendChild(li.childNodes[0]);
        compareProgressHolder.appendChild(compareProgressBar);
        compareProgressHolder.appendChild(compareProgressCircle);

        li.appendChild(compareProgressHolder);*/

        //1. nima nic
        //2. ima ikonoc
        //3. ima loader in ikono
        clearListItemIcon(compareEventLI, "iconHolder", "expendable");
        clearListItemProgressIndicator(compareEventLI, "loaderHolder", "expendable");
        let compareController = appendListItemProgressIndicator(compareEventLI, "loaderHolder", "loaderBar", "loaderCircle", "expendable");



        guiSubmissionTracks = removeGUI(guiSubmissionTracks);



        //initiate reading of submission file (with FILE READER)
        readSubmissionFile_FR(this.files[0], selectedEventID, compareController);
    });



    //CUSTOM EVENT SECTION

    //button
    let uploadCustomEventButton = {
        uploadCustomEvent: function(){
            //run file chooser
            document.getElementById('file_chooser_custom_event').click();
        }
    };

    //controller
    let uploadCustomEventController = guiEvents.add(uploadCustomEventButton, "uploadCustomEvent").name("Add events");
    uploadCustomEventController.domElement.parentNode.id = "uploadCustomEvent";
    let uploadCustomEventLI = uploadCustomEventController.domElement.parentNode.parentNode;

    //icon
    appendListItemIcon(uploadCustomEventLI, "iconHolder", "iconFolder", "expendable");

    //event listener
    document.getElementById("file_chooser_custom_event").addEventListener("change", function(){
        //loading progress


        //CLEAN
        //clear folder icon holder (prepare space for loading circle)
        clearListItemIcon(uploadCustomEventLI, "iconHolder", "expendable");
        //clear previous load progress indicator
        clearListItemBackground(uploadCustomEventLI, "backgroundHolder", "expendable");


        //ADD
        //append new loading progress indicator
        appendListItemProgressIndicator(uploadCustomEventLI, "loaderHolder", "loaderBar", "loaderCircle", "expendable");


        //initiate reading of event files (with FILE READER)
        for(let i = 0; i < eventList.length; i++){
            if(eventList[i].source === "client") {
                eventList.splice(i, 1);
                i--;
            }
        }

        customEventLoader.readCustomEventFile_FR(this.files, uploadCustomEventLI);

        for(key in customEventsFilesList){
            eventList.push({id: key, source: "client"});
        }

        updateGUIEventList(folderEvents, guiEventControllerList);
    });



    //HELP section
    var helpButton = {
        showHelpPopup: function(){
            //initiate help popup
            $( "#dialog-help" ).dialog({
                resizable: true,
                height: "auto",
                width: 640,
                modal: true,
                buttons: {
                    "Close": function() {
                        $( this ).dialog( "close" );
                    }
                }
            });

            //opacity (parent)
            $( "#dialog-help" ).parent().css( "opacity", 0.8 );
            $( "#dialog-help" ).parent().css( "filter", "alpha(opacity="+80+")" ); /* For IE8 and earlier */
        }
    };
    //add help button
    var helpButtonController = guiEvents.add(helpButton, "showHelpPopup").name("Help");



    //STAT TEST
    /*var statButton = {
        stat: function(){
            stat.initTest(10000, statLI);
        }
    };
    //stat button
    var statButtonController = guiEvents.add(statButton, "stat").name("Calc stats");
    let statLI = statButtonController.domElement.parentNode.parentNode;*/
}
function appendListItemIcon(li, iconHolderClass, iconClass, expendableClass){
    //if(iconHolder === undefined){
        //let li = controller.domElement.parentNode.parentNode;

        let iconHolder = document.createElement("DIV");
        let icon = document.createElement("DIV");

        icon.classList.add(iconClass, expendableClass);
        iconHolder.classList.add(iconHolderClass);

        //iconHolder.appendChild(li.childNodes[0]);
        /*for(let i = 0; i < li.childNodes.length; i++)
            iconHolder.appendChild(li.childNodes[i]);*/
        while (li.firstChild) {
            iconHolder.appendChild(li.firstChild);
        }
        iconHolder.appendChild(icon);

        li.appendChild(iconHolder);
    //}
}
function clearListItemIcon(li, iconHolderClass, expendableClass){
    //let li = controller.domElement.parentNode.parentNode.parentNode;    console.log(li.childNodes); //zaradi prevezave je li dodatni nivo visje
    let iconHolder = undefined;

    for(let i = 0; i < li.childNodes.length; i++){
        //if(li.childNodes[i].className === iconHolderClass) iconHolder = li.childNodes[i];
        if(li.childNodes[i].classList.contains(iconHolderClass)) iconHolder = li.childNodes[i];
    }

    if(iconHolder !== undefined){
        //iconHolder.parentElement.appendChild(iconHolder.childNodes[0]);
        /*for(let i = 0; i < iconHolder.childNodes.length; i++)
            iconHolder.parentElement.appendChild(iconHolder.childNodes[i]);*/
        while (iconHolder.firstChild) {
            if(iconHolder.firstChild.classList.contains(expendableClass)){
                iconHolder.removeChild(iconHolder.firstChild);
            }else {
                iconHolder.parentElement.appendChild(iconHolder.firstChild);
            }
        }

        iconHolder.remove();
        iconHolder = undefined; //clear self variable
    }
}
function appendListItemProgressIndicator(li, loaderHolderClass, loaderBarClass, loaderCircleClass, expendableClass, barColor, background){
    //let li = controller.domElement.parentNode.parentNode;

    let progressHolder = document.createElement("DIV");
    let progressBar = document.createElement("DIV");
    let progressCircle = document.createElement("DIV");

    progressCircle.classList.add(loaderCircleClass, expendableClass);
    progressBar.classList.add(loaderBarClass, expendableClass);
    progressHolder.classList.add(loaderHolderClass);

    if(barColor !== undefined) progressBar.style.background = barColor;
    if(background === true) progressBar.style.width = 100 + "%";

    //progressHolder.appendChild(li.childNodes[0]);
    /*for(let i = 0; i < li.childNodes.length; i++)
        progressHolder.appendChild(li.childNodes[i]);*/
    while (li.firstChild) {
        progressHolder.appendChild(li.firstChild);
    }
    progressHolder.appendChild(progressBar);
    progressHolder.appendChild(progressCircle);

    li.appendChild(progressHolder);


    return {progressHolder: progressHolder, progressBar: progressBar, progressCircle: progressCircle};
}
function clearListItemProgressIndicator(li, progressHolderClass, expendableClass){
    let progressHolder = undefined;

    for(let i = 0; i < li.childNodes.length; i++){
        //if(li.childNodes[i].className === progressHolderClass) progressHolder = li.childNodes[i];
        if(li.childNodes[i].classList.contains(progressHolderClass)) progressHolder = li.childNodes[i];
    }

    if(progressHolder !== undefined){
        //progressHolder.parentElement.appendChild(progressHolder.childNodes[0]);
        /*for(let i = 0; i < progressHolder.childNodes.length; i++)
            progressHolder.parentElement.appendChild(progressHolder.childNodes[i]);*/
        while (progressHolder.firstChild) {

            if(progressHolder.firstChild.classList.contains(expendableClass)){
                progressHolder.removeChild(progressHolder.firstChild);
            }else {
                progressHolder.parentElement.appendChild(progressHolder.firstChild);
            }

        }

        progressHolder.remove();
        progressHolder = undefined; //clear self variable
    }
}
function appendListItemBackground(li, backgroundHolderClass, backgroundBarClass, backgroundIconClass, expendableClass, barColor){
    appendListItemProgressIndicator(li, backgroundHolderClass, backgroundBarClass, backgroundIconClass, expendableClass, barColor, true);
}
function clearListItemBackground(li, backgroundHolderClass, expendableClass){
    clearListItemProgressIndicator(li, backgroundHolderClass, expendableClass);
}
function removeGUI(gui, controllerList){
    if(gui !== undefined) {
        /*gui.domElement.remove();
        gui = undefined;
        controllerList = []; //list of current tracks in gui*/

        gui.destroy();
        return undefined;
    }
}
function updateGUIEventList(folderEvents, controllers){
    if(controllers !== undefined){
        while(controllers.length > 0) folderEvents.remove(controllers.pop());
    }

    for(var i = guiEventListSection*guiEventsPerSection; i < guiEventListSection*guiEventsPerSection + guiEventsPerSection; i++){
        if(i === eventList.length) break;

        addGUIEventButton(eventList, i, folderEvents, controllers);
    }
}
function updateGUISearchEventList(folderEvents, controllers){
    if(controllers !== undefined){
        while(controllers.length > 0) folderEvents.remove(controllers.pop());
    }

    for(var i = guiSearchEventListSection*guiSearchEventsPerSection; i < guiSearchEventListSection*guiSearchEventsPerSection + guiSearchEventsPerSection; i++){
        if(i === searchEventList.length) break;

        addGUIEventButton(searchEventList, i, folderEvents, controllers);
    }
}
function addGUIEventButton(eventList, i, folderEvents, controllers){
    var EventButton = {
        id: eventList[i].id,
        index: i % guiEventsPerSection,
        source: eventList[i].source,
        clicked: false,
        clickEventButton: function(){
            //On click do:
            //console.log("clicked event id: " + this.id);



            //VARS
            this.clicked = !this.clicked;
            eventLoaded = false;
            //set ID
            selectedEventID = this.id;



            //CLEANUP
            //clear previously colored background
            for(let i = 0; i < controllers.length; i++) {
                //clear selection flag from all buttons in display list, except current button
                if(controllers[i].object.id !== this.id) controllers[i].object.clicked = false;


                if(controllers[i].domElement.parentNode.parentNode.className === "loaderHolder"){
                    clearListItemProgressIndicator(controllers[i].object.eventLI, "loaderHolder", "expendable");
                }
            }


            //clear gui
            guiTracks = removeGUI(guiTracks); //gui with hits and tracks (change name for better reflection?)
            //clear submission tracks gui
            guiSubmissionTracks = removeGUI(guiSubmissionTracks);


            //clear scene
            //remove any previous objects
            scene.remove(scene.getObjectByName("Particles"));
            scene.remove(scene.getObjectByName("Hits"));
            scene.remove(scene.getObjectByName("Tracks"));
            scene.remove(scene.getObjectByName("SelectedTrack"));
            scene.remove(scene.getObjectByName("SubmissionTracks"));
            scene.remove(scene.getObjectByName("SelectedSubmissionTrack"));


            //clear params
            params.selectedTrackID = -1;
            params.selectedHitID = -1;
            track_selected = false;
            submitted_track_selected = false;

            //clean gui markers
            clearListItemIcon(g_compareEventLI, "iconHolder", "expendable");
            clearListItemProgressIndicator(g_compareEventLI, "loaderHolder", "expendable");
            //appendListItemIcon(g_compareEventLI, "iconHolder", "iconFolder", "expendable");

            g_CompareButtonController.name("Choose an event from list"); //order important!!!



            //INIT
            if(this.clicked === true){
                let progressController = appendListItemProgressIndicator(this.eventLI, "loaderHolder", "loaderBar", "loaderCircle", "expendable");


                //initiate reading of collision files (ASYNC!!!)
                if(this.source === "server") {
                    readCollisionEventFiles(this.id, progressController);
                }else if(this.source === "client"){
                    readCollisionEventFiles_FR(this.id, progressController);
                }


            }




            //deprecation
            //clear submission compare flag
            //params.compareSubmissionData = false;


            //clear submission gui init flag
            //submissionTrackGUInitialized = false;




            //load progress
            //clear previous load progress indicator
            /*if(progressHolder !== undefined) {
                progressHolder.parentElement.appendChild(progressHolder.childNodes[0]);
                progressHolder.remove();
                progressHolder = undefined; //clear self variable
            }*/



            //clear previous compare load progress indicator
            /*if(compareProgressHolder !== undefined){
                compareProgressHolder.parentElement.appendChild(compareProgressHolder.childNodes[0]);
                compareProgressHolder.remove();
                compareProgressHolder = undefined; //clear self variable


                //add folder icon
                if(compareFolderIconHolder === undefined){
                    var li = g_CompareButtonController.domElement.parentNode.parentNode;

                    compareFolderIconHolder = document.createElement("DIV");
                    compareFolderIcon = document.createElement("DIV");

                    compareFolderIcon.classList.add("iconFolder");
                    compareFolderIconHolder.classList.add("iconHolder");

                    compareFolderIconHolder.appendChild(li.childNodes[0]);
                    compareFolderIconHolder.appendChild(compareFolderIcon);

                    li.appendChild(compareFolderIconHolder);
                }
            }*/






            //append new load progress indicator
            //var li = document.getElementById("events").childNodes[0].childNodes[this.index + 4]; // +4 to compensate for nav buttons and folder name
            //var li = folderEvents.domElement.childNodes[0].childNodes[this.index + 4];
            /*var li = eventController.domElement.parentNode.parentNode;

            progressHolder = document.createElement("DIV");
            progressBar = document.createElement("DIV");
            progressCircle = document.createElement("DIV");

            progressCircle.classList.add("loaderCircle");
            progressBar.classList.add("loaderBar");
            progressHolder.classList.add("loaderHolder");

            progressHolder.appendChild(li.childNodes[0]);
            progressHolder.appendChild(progressBar);
            progressHolder.appendChild(progressCircle);

            li.appendChild(progressHolder);*/


        }
    };

    var eventController = folderEvents.add(EventButton, "clickEventButton").name("Event ID: " + eventList[i].id);
    controllers.push(eventController);
    EventButton.eventLI = eventController.domElement.parentNode.parentNode;
}

function addGUITrackList(){
    guiTracks = new dat.GUI();
    guiTracks.domElement.id = "GUITracks";

    //Tracks list selection
    folderTracks = guiTracks.addFolder("Tracks");
    //folderTracks = gui.addFolder("Tracks");
    let guiTrackControllerList = [];
    g_guiTrackControllerList = guiTrackControllerList;

    var NavButton = {
        clickPrevButton: function(){
            if(guiTrackListSection > 0) {
                guiTrackListSection--;
                //updateGUITrackList(folderTracks, guiTrackControllerList);
            }
            updateGUITrackList(trackList, Object.keys(trackList), folderTracks, guiTrackControllerList);
        },
        clickNextButton: function(){
            if(guiTrackListSection < Object.keys(trackList).length/guiTracksPerSection - 1) {
                guiTrackListSection++;
                //updateGUITrackList(folderTracks, guiTrackControllerList);
            }
            updateGUITrackList(trackList, Object.keys(trackList), folderTracks, guiTrackControllerList);
        }
    };

    var cont1 = folderTracks.add(NavButton, "clickPrevButton").name("Prev");
    var cont2 = folderTracks.add(NavButton, "clickNextButton").name("Next");
    var cont3 = folderTracks.add(params, "searchTrackID");

    folderTracks.domElement.id = "tracks";
    cont1.domElement.parentNode.parentNode.id = "trackListNav";
    cont2.domElement.parentNode.parentNode.id = "trackListNav";
    cont3.domElement.parentNode.id = "trackListSearch";


    //search option
    cont3.onChange(function(value){
        //multiple match
        var found = Object.keys(trackList).filter(function(element){ //empty array ce ne najde nic
            return element.includes(value);
        });

        searchTrackList = {};
        let searchTrackIDList = [];
        if(found.length > 0){
            guiSearchTrackListSection = 0;
            //clear current list
            //searchTrackList = {};
            //add to list
            for(var i = 0; i < found.length; i++) {
                searchTrackIDList.push(found[i]);
                searchTrackList[found[i]] = trackList[found[i]];  //creates object   //obj[key] = someValue;
            }
        }
        updateGUISearchTrackList(searchTrackList, searchTrackIDList, folderTracks, guiTrackControllerList);
    });


    folderTracks.open();
    updateGUITrackList(trackList, Object.keys(trackList), folderTracks, guiTrackControllerList);
}
function addGUISubmissionTrackList(){
    guiSubmissionTracks = new dat.GUI();
    guiSubmissionTracks.domElement.id = "GUITracks";

    //Tracks list selection
    folderSubmissionTracks = guiSubmissionTracks.addFolder("SubmissionTracks");
    let guiSubmissionTrackControllerList = [];


    var NavButton = {
        clickPrevButton: function(){
            if(guiTrackListSection > 0) {
                guiTrackListSection--;
            }
            updateGUITrackList(submissionTrackList, Object.keys(submissionTrackList), folderSubmissionTracks, guiSubmissionTrackControllerList);
        },
        clickNextButton: function(){
            if(guiTrackListSection < Object.keys(submissionTrackList).length/guiTracksPerSection - 1) {
                guiTrackListSection++;
            }
            updateGUITrackList(submissionTrackList, Object.keys(submissionTrackList), folderSubmissionTracks, guiSubmissionTrackControllerList);
        }
    };

    var cont1 = folderSubmissionTracks.add(NavButton, "clickPrevButton").name("Prev");
    var cont2 = folderSubmissionTracks.add(NavButton, "clickNextButton").name("Next");
    var cont3 = folderSubmissionTracks.add(params, "searchTrackID");

    folderSubmissionTracks.domElement.id = "SubmissionTracks";
    cont1.domElement.parentNode.parentNode.id = "trackListNav";
    cont2.domElement.parentNode.parentNode.id = "trackListNav";
    cont3.domElement.parentNode.id = "trackListSearch";


    //search option
    cont3.onChange(function(value){
        //multiple match
        var found = Object.keys(submissionTrackList).filter(function(element){ //empty array ce ne najde nic
            return element.includes(value);
        });

        //clear current list
        searchTrackList = {};
        let searchTrackIDList = [];
        if(found.length > 0){
            guiSearchTrackListSection = 0;

            //add to list
            for(var i = 0; i < found.length; i++) {
                searchTrackIDList.push(found[i]);
                searchTrackList[found[i]] = submissionTrackList[found[i]];  //creates object   //obj[key] = someValue;
            }
        }
        updateGUISearchTrackList(searchTrackList, searchTrackIDList, folderSubmissionTracks, guiSubmissionTrackControllerList);
    });


    folderSubmissionTracks.open();
    updateGUITrackList(submissionTrackList, Object.keys(submissionTrackList), folderSubmissionTracks, guiSubmissionTrackControllerList);
}
function updateGUITrackList(trackList, trackIDList, folderTracks, guiTrackControllerList){
    if(guiTrackControllerList !== undefined){
        while(guiTrackControllerList.length > 0) folderTracks.remove(guiTrackControllerList.pop());
    }

    for(var i = guiTrackListSection*guiTracksPerSection; i < guiTrackListSection*guiTracksPerSection + guiTracksPerSection; i++){
        if(i === Object.keys(trackList).length) break;

        addGUITrackButton(trackList, trackIDList[i], folderTracks, guiTrackControllerList);
    }
}
function updateGUISearchTrackList(searchTrackList, searchTrackIDList, folderTracks, guiTrackControllerList){
    if(guiTrackControllerList !== undefined){
        while(guiTrackControllerList.length > 0) folderTracks.remove(guiTrackControllerList.pop());
    }

    for(var i = guiSearchTrackListSection*guiSearchTracksPerSection; i < guiSearchTrackListSection*guiSearchTracksPerSection + guiSearchTracksPerSection; i++){
        if(i === Object.keys(searchTrackList).length) break;

        addGUITrackButton(searchTrackList, searchTrackIDList[i], folderTracks, guiTrackControllerList);
    }
}
let track_selected = false;
let submitted_track_selected = false;
function addGUITrackButton(trackList, trackID, folderTracks, guiTrackControllerList){
    var TrackButton = {
        //id: trackList[index].hit_id,
        //id: Object.keys(trackList)[index],
        id: trackID,
        clicked: false,
        clickTrackButton: function(){
            //On click do:
            //console.log("clicked track id: " + this.id);


            //change color of clicked/selected item
            //1. clear previously colored background
            var prevBackgroundHolder;
            for(var i = 0; i < guiTrackControllerList.length; i++) {
                //clear selection flag from all buttons in display list, except current button
                if(guiTrackControllerList[i].object.id !== this.id) guiTrackControllerList[i].object.clicked = false;


                if(guiTrackControllerList[i].domElement.parentNode.parentNode.className === "backgroundHolder"){
                    prevBackgroundHolder = guiTrackControllerList[i].domElement.parentNode.parentNode;
                    //rewire
                    prevBackgroundHolder.parentElement.appendChild(prevBackgroundHolder.childNodes[0]);
                    //delete
                    prevBackgroundHolder.remove();
                }
            }


            this.clicked = !this.clicked;
            if(this.clicked === false){
                if(folderTracks.domElement.id === "SubmissionTracks"){
                    scene.remove(scene.getObjectByName("SelectedSubmissionTrack"));

                    submitted_track_selected = false;
                }else if(folderTracks.domElement.id === "tracks"){
                    scene.remove(scene.getObjectByName("SelectedTrack"));

                    track_selected = false;
                }

                //-1 == deselected
                if(track_selected === false && submitted_track_selected === false) {
                    //shader uniform
                    params.selectedTrackID = -1;
                    hitsObject.material.uniforms.selectedTrackID.value = -1;
                    particlesObject.material.uniforms.selectedTrackID.value = -1;
                    tracksObject.material.uniforms.selectedTrackID.value = -1;
                    if (submittedTracksObject !== undefined) submittedTracksObject.material.uniforms.selectedTrackID.value = -1;
                }else if(track_selected === false && submitted_track_selected === true){
                    //shader uniform
                    params.selectedTrackID = -2;
                    hitsObject.material.uniforms.selectedTrackID.value = -2;
                    particlesObject.material.uniforms.selectedTrackID.value = -2;
                    tracksObject.material.uniforms.selectedTrackID.value = -2;
                    if (submittedTracksObject !== undefined) submittedTracksObject.material.uniforms.selectedTrackID.value = -2;
                }


                return;
            }

            //2. add color to newly selected item
            var li = trackController.domElement.parentNode.parentNode;

            var backgroundHolder = document.createElement("DIV");
            var colorBar = document.createElement("DIV");

            if(folderTracks.domElement.id === "SubmissionTracks"){
                colorBar.classList.add("submissionTrackColorBar");
                colorBar.style.backgroundColor = '#' + ("000000" + params.selectedSubmissionTrackColor.toString(16)).substr(-6);

                submitted_track_selected = true;
            }else if(folderTracks.domElement.id === "tracks"){
                colorBar.classList.add("trackColorBar");
                colorBar.style.backgroundColor = '#' + ("000000" + params.selectedTrackColor.toString(16)).substr(-6);

                track_selected = true;
            }
            backgroundHolder.classList.add("backgroundHolder");

            backgroundHolder.appendChild(li.childNodes[0]);
            backgroundHolder.appendChild(colorBar);

            li.appendChild(backgroundHolder);


            if(folderTracks.domElement.id === "tracks") {
                //shader uniform
                params.selectedTrackID = this.id;
                //hitsObject.material.uniforms.selectedTrackID.value = this.id;
                //particlesObject.material.uniforms.selectedTrackID.value = this.id;
                //tracksObject.material.uniforms.selectedTrackID.value = this.id;
                hitsObject.material.uniforms.selectedTrackID.value = g_trackIDMap[this.id];
                particlesObject.material.uniforms.selectedTrackID.value = g_trackIDMap[this.id];
                tracksObject.material.uniforms.selectedTrackID.value = g_trackIDMap[this.id];
                if (submittedTracksObject !== undefined) submittedTracksObject.material.uniforms.selectedTrackID.value = this.id;
            }else if(folderTracks.domElement.id === "SubmissionTracks") {
                if(track_selected === false) {
                    //shader uniform
                    params.selectedTrackID = -2;
                    hitsObject.material.uniforms.selectedTrackID.value = -2;
                    particlesObject.material.uniforms.selectedTrackID.value = -2;
                    tracksObject.material.uniforms.selectedTrackID.value = -2;
                    if (submittedTracksObject !== undefined) submittedTracksObject.material.uniforms.selectedTrackID.value = -2;
                }
            }


                //load selected track with width
            //loadSelectedTrack(this.id, params.widthSelectedTrack); //gl_LineWidth
            //loadSelectedTrack2(this.id, params.widthSelectedTrack); //eqdistance points
            //loadSelectedTrack3(this.id, params.widthSelectedTrack); //simple strip
            if(folderTracks.domElement.id === "SubmissionTracks"){
                loadSelectedTrack31(trackList, this.id, "SelectedSubmissionTrack"); //NCD
            }else if(folderTracks.domElement.id === "tracks"){
                loadSelectedTrack31(trackList, this.id, "SelectedTrack"); //NCD
            }
            //loadSelectedTrack32(this.id, params.widthSelectedTrack); //MeshLine library (face culling)


            if(folderTracks.domElement.id === "SubmissionTracks") {
                //INITIATE SIMILAR TRACK SEARCH
                //search option
                //multiple match
                const selectedSubmissionTrack = trackList[this.id];
                let similarities = [];

                const centroid1 = calculateCentroid(selectedSubmissionTrack);
                const centroid1V = new THREE.Vector3(centroid1[0], centroid1[1], centroid1[2]);
                let centroid2;
                let centroid2V;
                let track;
                for(let trackID in window.trackList){
                    track = getRawTrack(window.trackList[trackID]); // remove initial particle in core tracks representation

                    //compute distance
                    centroid2 = calculateCentroid(track);
                    centroid2V = new THREE.Vector3(centroid2[0], centroid2[1], centroid2[2]);

                    let distance = centroid1V.distanceTo(centroid2V);
                    let n_hits_diff = Math.abs(selectedSubmissionTrack.length - track.length);
                    similarities.push({track_id: trackID, distance: distance, n_hits_diff: n_hits_diff});
                }

                //sort similarities
                similarities.sort(function(a, b){
                    //return (a.distance - b.distance) + (a.n_hits_diff - b.n_hits_diff); //same as below
                    return (a.distance + a.n_hits_diff) - (b.distance + b.n_hits_diff);//hits difference
                });
                ///console.log(similarities);


                //clear current list
                searchTrackList = {};
                let searchTrackIDList = [];
                if (similarities.length > 0) {
                    //if match exists
                    guiSearchTrackListSection = 0;

                    //add to list
                    for (let i = 0; i < similarities.length; i++) {
                        searchTrackIDList.push(similarities[i].track_id);
                        searchTrackList[similarities[i].track_id] = window.trackList[similarities[i].track_id];
                    }

                    updateGUITrackList(searchTrackList, searchTrackIDList, window.folderTracks, g_guiTrackControllerList);
                } else {
                    //if no match
                    updateGUITrackList(searchTrackList, searchTrackIDList, window.folderTracks, g_guiTrackControllerList);
                }
            }



            //focus camera on track
            var centroid = calculateCentroid(trackList[this.id]);


            var trackStartPoint = new THREE.Vector3(trackList[this.id][0].tx, trackList[this.id][0].ty, trackList[this.id][0].tz);
            var trackEndPoint = new THREE.Vector3(trackList[this.id][trackList[this.id].length-1].tx, trackList[this.id][trackList[this.id].length-1].ty, trackList[this.id][trackList[this.id].length-1].tz);
            var EndStartVector = new THREE.Vector3();
            EndStartVector.subVectors(trackEndPoint, trackStartPoint);
            var EndStartVectorDiv2 = new THREE.Vector3(EndStartVector.x, EndStartVector.y, EndStartVector.z);
            EndStartVectorDiv2.divideScalar(2);

            var c = new THREE.Vector3();
            c.addVectors(trackStartPoint, EndStartVectorDiv2);

            var l = new THREE.Vector3();
            //l.subVectors(centroid, c);
            l.x = centroid[0] - c.x;
            l.y = centroid[1] - c.y;
            l.z = centroid[2] - c.z;

            var d = new THREE.Vector3();
            d.crossVectors(EndStartVectorDiv2, l);
            d.normalize();
            d.multiplyScalar(EndStartVectorDiv2.length()); //off distance factor

            var cameraPosition = new THREE.Vector3();
            cameraPosition.addVectors(c, d);

            var targetPosition = new THREE.Vector3();
            //target vertex
            targetPosition.x = centroid[0];
            targetPosition.y = centroid[1];
            targetPosition.z = centroid[2];
            //target vector
            //target.x = centroid[0] - position.x;
            //target.y = centroid[1] - position.y;
            //target.z = centroid[2] - position.z;


            if(trackList[this.id].length === 1){//if track lenght is 1 == same as hit todo opt
                cameraPosition.x = trackList[this.id][0].tx;
                cameraPosition.y = trackList[this.id][0].ty;
                cameraPosition.z = trackList[this.id][0].tz;
                cameraPosition.multiplyScalar(0.85); //85% of length to target position
            }
            cameraFocusPosition = cameraPosition;
            cameraTargetFocusPosition = targetPosition;
            focusCamera = true;
        }
    };


    var trackController = folderTracks.add(TrackButton, "clickTrackButton").name("Track ID: " + trackID);
    guiTrackControllerList.push(trackController);
}

function addGUIHitList(){
    //var guiHits = new dat.GUI();
    //guiHits.domElement.id = "GUIHits";

    //Hit list selection
    folderHits = guiTracks.addFolder("Hits");
    let guiHitControllerList = [];


    var NavButton = {
        clickPrevButton: function(){
            if(guiHitListSection > 0) {
                guiHitListSection--;
            }
            updateGUIHitList(folderHits, guiHitControllerList);
        },
        clickNextButton: function(){
            if(guiHitListSection < hitList.length/guiHitsPerSection - 1) {
                guiHitListSection++;
            }
            updateGUIHitList(folderHits, guiHitControllerList);
        }
    };

    var cont1 = folderHits.add(NavButton, "clickPrevButton").name("Prev");
    var cont2 = folderHits.add(NavButton, "clickNextButton").name("Next");
    var cont3 = folderHits.add(params, "searchHitID");

    folderHits.domElement.id = "hits";
    cont1.domElement.parentNode.parentNode.id = "hitListNav";
    cont2.domElement.parentNode.parentNode.id = "hitListNav";
    cont3.domElement.parentNode.id = "hitListSearch";


    //search option
    cont3.onChange(function(value){
        //multiple match
        var found = hitList.filter(function(element){ //empty array ce ne najde nic
            return element.hit_id.toString().includes(value);
        });

        if(found.length > 0){
            //if match

            //reassign list
            searchHitList = found;

        }else{
            //if no match

            //clear current list
            searchHitList = [];
        }
        guiSearchHitListSection = 0;
        updateGUISearchHitList(folderHits, guiHitControllerList);
    });


    //folderHits.open();
    updateGUIHitList(folderHits, guiHitControllerList);
}
function updateGUIHitList(folderHits, guiHitControllerList){
    if(guiHitControllerList !== undefined){
        while(guiHitControllerList.length > 0) folderHits.remove(guiHitControllerList.pop());
    }

    for(var i = guiHitListSection*guiHitsPerSection; i < guiHitListSection*guiHitsPerSection + guiHitsPerSection; i++){
        if(i === hitList.length) break;

        addGUIHitButton(hitList, i, folderHits, guiHitControllerList);
    }
}
function updateGUISearchHitList(folderHits, guiHitControllerList){
    if(guiHitControllerList !== undefined){
        while(guiHitControllerList.length > 0) folderHits.remove(guiHitControllerList.pop());
    }

    for(var i = guiSearchHitListSection*guiSearchHitsPerSection; i < guiSearchHitListSection*guiSearchHitsPerSection + guiSearchHitsPerSection; i++){
        if(i === searchHitList.length) break;

        addGUIHitButton(searchHitList, i, folderHits, guiHitControllerList);
    }
}
function addGUIHitButton(hitList, index, folderHits, guiHitControllerList){
    var HitButton = {
        index: index,
        id: hitList[index].hit_id,
        clicked: false,
        clickHitButton: function(){
            //On click do:
            //console.log("clicked hit id: " + this.id);


            //change color of clicked/selected item
            //1. clear previously colored background
            var prevBackgroundHolder;
            for(var i = 0; i < guiHitControllerList.length; i++) {
                //clear selection flag from all buttons in display list, except current button
                if(guiHitControllerList[i].object.id !== this.id) guiHitControllerList[i].object.clicked = false;


                if(guiHitControllerList[i].domElement.parentNode.parentNode.className === "backgroundHolder"){
                    prevBackgroundHolder = guiHitControllerList[i].domElement.parentNode.parentNode;
                    //rewire
                    prevBackgroundHolder.parentElement.appendChild(prevBackgroundHolder.childNodes[0]);
                    //delete
                    prevBackgroundHolder.remove();
                }
            }

            this.clicked = !this.clicked;
            if(this.clicked === false){
                //params and shader uniforms
                params.selectedHitID = -1;
                hitsObject.material.uniforms.selectedHitID.value = -1;


                return;
            }

            //2. add color to newly selected item
            var li = hitController.domElement.parentNode.parentNode;

            var backgroundHolder = document.createElement("DIV");
            var colorBar = document.createElement("DIV");

            colorBar.classList.add("hitColorBar");
            backgroundHolder.classList.add("backgroundHolder");

            colorBar.style.backgroundColor = '#' + ("000000" + params.selectedHitColor.toString(16)).substr(-6);

            backgroundHolder.appendChild(li.childNodes[0]);
            backgroundHolder.appendChild(colorBar);

            li.appendChild(backgroundHolder);


            //params and shader uniforms
            params.selectedHitID = this.id;
            hitsObject.material.uniforms.selectedHitID.value = this.id;


            //focus camera on hit
            var cameraPosition = new THREE.Vector3();
            cameraPosition.x = hitList[this.index].x;
            cameraPosition.y = hitList[this.index].y;
            cameraPosition.z = hitList[this.index].z;
            cameraPosition.multiplyScalar(0.85); //85% of length to target position

            var targetPosition = new THREE.Vector3();
            targetPosition.x = hitList[this.index].x;
            targetPosition.y = hitList[this.index].y;
            targetPosition.z = hitList[this.index].z;


            cameraFocusPosition = cameraPosition;
            cameraTargetFocusPosition = targetPosition;
            focusCamera = true;
        }
    };

    var hitController = folderHits.add(HitButton, "clickHitButton").name("Hit ID: " + hitList[index].hit_id);
    guiHitControllerList.push(hitController);
}


function calculateCentroid(array){
    var position = [0, 0, 0];

    for(var i = 0; i < array.length; i++){
        position[0] += array[i].tx;
        position[1] += array[i].ty;
        position[2] += array[i].tz;
    }
    position[0] /= array.length;
    position[1] /= array.length;
    position[2] /= array.length;

    return position;
}
function calculateTracksDistance(track1, track2){
    if(track1[0].hit_id === -1)

        var minPoints;
    if(track1.length <= track2.length){
        minPoints = track1.length;
    }else{
        minPoints = track2.length;
    }

    //console.log(track1);
    //console.log(track2);

    var vertexDistance = 0;
    var vertexTrack1 = THREE.Vector3();
    var vertexTrack2 = THREE.Vector3();

    var centroid1 = calculateCentroid(track1);
    var centroid2 = calculateCentroid(track2);

    return Math.abs(centroid1 - centroid2);
}
function getRawTrack(track){
    let rawTrack = [];

    for(let i = 1; i < track.length; i++){
        rawTrack.push(track[i]);
    }

    return rawTrack;
}