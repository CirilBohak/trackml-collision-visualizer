function csvToJSON(csv){
    var lines = csv.split('\n');
    var header = lines[0].split(',');
    var currentLine;
    var result = [];


    for(var i = 1; i < lines.length; i++){
        var object = {};
        currentLine = lines[i].split(',');

        //check for splitting an empty line
        if(currentLine.length < header.length) {
            //console.log(currentLine);
            continue;
        }

        for(var j = 0; j < header.length; j++){
            if(header[j].includes("id")) {
                object[header[j]] = currentLine[j];
            }else {
                object[header[j]] = JSON.parse(currentLine[j]); //JSON.parse() remove "" quotes
            }
        }

        result.push(object);
    }

    //return result; //JavaScript object
    return JSON.stringify(result); //JSON object
}


function updateProgress (oEvent) {
    if (oEvent.lengthComputable) {

    } else {
        // Unable to compute progress information since the total size is unknown
    }
}

function transferComplete(evt) {
    console.log("The transfer is complete.");
}

function transferFailed(evt) {
    console.log("An error occurred while transferring the file.");
}

function transferCanceled(evt) {
    console.log("The transfer has been canceled by the user.");
}