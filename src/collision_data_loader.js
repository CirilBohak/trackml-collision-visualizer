var numCollisionDataFiles = 3;
var loadedCollisionDataFiles = 0;
var progressHolder;
var progressBar;
var progressCircle;
var progressPerFiles = new Array(numCollisionDataFiles).fill(0);
let collisionDataReadError = false;


// progress on transfers from the server to the client (downloads)
function eventFilesUpdateProgress (oEvent) {
    if (oEvent.lengthComputable) {
        var percentComplete = oEvent.loaded / oEvent.total * 100;

        if(oEvent.target.responseURL.includes("particles")) {
            progressPerFiles[0] = percentComplete;
        }else if(oEvent.target.responseURL.includes("hits")) {
            progressPerFiles[1] = percentComplete;
        }else if(oEvent.target.responseURL.includes("truth")) {
            progressPerFiles[2] = percentComplete;
        }
        //console.log(oEvent);
        //console.log(oEvent.target.responseURL);
        //console.log(percentComplete);
        //progressBar.style.width = percentComplete+"%";
        progressBar.style.width = ((progressPerFiles[0] + progressPerFiles[1] + progressPerFiles[2])/numCollisionDataFiles) + "%";
    } else {
        // Unable to compute progress information since the total size is unknown
    }
}
function eventFilesUpdateProgress_FR (oEvent) {
    if (oEvent.lengthComputable) {
        collisionDataLoaded += oEvent.loaded;
        let percentComplete = collisionDataLoaded / collisionDataTotal * 100;
        collisionDataTotal += oEvent.loaded;

        progressBar.style.width = percentComplete + "%";
    } else {
        // Unable to compute progress information since the total size is unknown
    }
}
function eventFilesTransferComplete(evt) {//goes to onload
    transferComplete(evt);
}
function eventFilesTransferFailed(evt) {
    transferFailed(evt);


    loadedCollisionDataFiles++;

    collisionDataReadError = true;

    if(loadedCollisionDataFiles === numCollisionDataFiles){
        loadedCollisionDataFiles = 0;


        //progressPerFiles.fill(0);
        progressCircle.remove();
        progressBar.style.width = 100 + "%";
        progressBar.style.background = params.colorErr;

        //notify
        let message = "Error reading collision data!";
        console.log(message);
        $("#GUIEvents").notify(message, { position:"bottom left", className:"error" });
    }
}
function eventFilesTransferCanceled(evt) {
    transferCanceled(evt);
}



function readCollisionEventFiles(eventID, progressController){
    collisionDataReadError = false;
    progressBar = progressController.progressBar;
    progressCircle = progressController.progressCircle;
    let particlesData = [], hitsData = [], cellsData = [], truthData = [];
    progressPerFiles.fill(0);


    //read collision data
    var particlesReq = new XMLHttpRequest();
    particlesReq.onload = function(e) {
        //load complete
        loadedCollisionDataFiles++;


        if (collisionDataReadError === false && this.readyState === 4 && this.status === 200) {
            var data = particlesReq.response;
            ///console.log(data);
            ///console.log(JSON.parse(data));
            ///console.log(JSON.parse(csvToJSON(data)));

            //particlesData = JSON.parse(data);
            particlesData = JSON.parse(csvToJSON(data));



            if(loadedCollisionDataFiles === numCollisionDataFiles){
                loadedCollisionDataFiles = 0;


                //progressPerFiles.fill(0);
                //progressHolder.parentElement.appendChild(progressHolder.childNodes[0]);
                //progressHolder.remove();
                progressCircle.remove();
                progressBar.style.background = params.colorOK;


                loadCollisionDataV2(particlesData, hitsData, cellsData, truthData);
            }
        }else{
            collisionDataReadError = true;


            if(loadedCollisionDataFiles === numCollisionDataFiles){
                loadedCollisionDataFiles = 0;


                //progressPerFiles.fill(0);
                progressCircle.remove();
                progressBar.style.background = params.colorErr;

                //notify
                let message = "Error reading collision data: " + this.status;
                console.log(message);
                $("#GUIEvents").notify(message, { position:"bottom left", className:"error" });
            }
        }
    };

    particlesReq.addEventListener("progress", eventFilesUpdateProgress);
    particlesReq.addEventListener("load", eventFilesTransferComplete);
    particlesReq.addEventListener("error", eventFilesTransferFailed);
    particlesReq.addEventListener("abort", eventFilesTransferCanceled);

    //particlesReq.open("GET", "./data/json/event"+eventID+"-particles.json", true);
    particlesReq.open("GET", "./data/csv/train_sample/event"+eventID+"-particles.csv", true);
    particlesReq.send();


    var hitsReq = new XMLHttpRequest();
    hitsReq.onload = function(e) {
        //load complete
        loadedCollisionDataFiles++;


        if (collisionDataReadError === false && this.readyState === 4 && this.status === 200) {
            var data = hitsReq.response;

            //hitsData = JSON.parse(data);
            hitsData = JSON.parse(csvToJSON(data));


            if (loadedCollisionDataFiles === numCollisionDataFiles) {
                loadedCollisionDataFiles = 0;


                //progressPerFiles.fill(0);
                progressCircle.remove();
                progressBar.style.background = params.colorOK;


                loadCollisionDataV2(particlesData, hitsData, cellsData, truthData);
            }
        }else{
            collisionDataReadError = true;


            if(loadedCollisionDataFiles === numCollisionDataFiles){
                loadedCollisionDataFiles = 0;


                //progressPerFiles.fill(0);
                progressCircle.remove();
                progressBar.style.background = params.colorErr;

                //notify
                let message = "Error reading collision data: " + this.status;
                console.log(message);
                $("#GUIEvents").notify(message, { position:"bottom left", className:"error" });
            }
        }
    };

    hitsReq.addEventListener("progress", eventFilesUpdateProgress);
    hitsReq.addEventListener("load", eventFilesTransferComplete);
    hitsReq.addEventListener("error", eventFilesTransferFailed);
    hitsReq.addEventListener("abort", eventFilesTransferCanceled);

    //hitsReq.open("GET", "./data/json/event"+eventID+"-hits.json", true);
    hitsReq.open("GET", "./data/csv/train_sample/event"+eventID+"-hits.csv", true);
    hitsReq.send();


    var truthReq = new XMLHttpRequest();
    truthReq.onload = function(e) {
        //load complete
        loadedCollisionDataFiles++;


        if (collisionDataReadError === false && this.readyState === 4 && this.status === 200) {
            var data = truthReq.response;

            //truthData = JSON.parse(data);
            truthData = JSON.parse(csvToJSON(data));


            if (loadedCollisionDataFiles === numCollisionDataFiles) {
                loadedCollisionDataFiles = 0;


                //progressPerFiles.fill(0);
                progressCircle.remove();
                progressBar.style.background = params.colorOK;


                loadCollisionDataV2(particlesData, hitsData, cellsData, truthData);
            }
        }else{
            collisionDataReadError = true;


            if(loadedCollisionDataFiles === numCollisionDataFiles){
                loadedCollisionDataFiles = 0;


                //progressPerFiles.fill(0);
                progressCircle.remove();
                progressBar.style.background = params.colorErr;

                //notify
                let message = "Error reading collision data: " + this.status;
                console.log(message);
                $("#GUIEvents").notify(message, { position:"bottom left", className:"error" });
            }
        }
    };

    truthReq.addEventListener("progress", eventFilesUpdateProgress);
    truthReq.addEventListener("load", eventFilesTransferComplete);
    truthReq.addEventListener("error", eventFilesTransferFailed);
    truthReq.addEventListener("abort", eventFilesTransferCanceled);

    //truthReq.open("GET", "./data/json/event"+eventID+"-truth.json", true);
    truthReq.open("GET", "./data/csv/train_sample/event"+eventID+"-truth.csv", true);
    truthReq.send();
}
let collisionDataLoaded;
let collisionDataTotal;
function readCollisionEventFiles_FR(eventID, progressController){
    collisionDataReadError = false;
    progressBar = progressController.progressBar;
    progressCircle = progressController.progressCircle;

    //check
    if(Object.keys(customEventsFilesList[eventID]).length < numCollisionDataFiles) {
        progressBar.style.width = 100 + "%";
        progressBar.style.background = params.colorErr;
        progressCircle.remove();

        //notify
        let message = "Missing files in event: " + eventID + "!";
        //console
        console.log(message);
        //popup
        $("#GUIEvents").notify(message, { position:"bottom left", className:"error" });
        return;
    }

    let particlesData = [], hitsData = [], cellsData = [], truthData = [];
    collisionDataLoaded = 0;
    collisionDataTotal = customEventsFilesList[eventID].particles.size + customEventsFilesList[eventID].hits.size + customEventsFilesList[eventID].truth.size;


    //PARTICLES
    var particlesFR = new FileReader();
    particlesFR.readAsText(customEventsFilesList[eventID].particles);
    particlesFR.onload = function(){
        //load complete
        //loadCompleteSignal(particlesData, hitsData, cellsData, truthData);
        loadedCollisionDataFiles++;


        if (collisionDataReadError === false) {
            var data = particlesFR.result;
            particlesData = JSON.parse(csvToJSON(data));



            if (loadedCollisionDataFiles === numCollisionDataFiles) {
                loadedCollisionDataFiles = 0;


                progressCircle.remove();
                progressBar.style.width = 100 + "%";
                progressBar.style.background = params.colorOK;

                loadCollisionDataV2(particlesData, hitsData, cellsData, truthData);
            }
        }else{
            collisionDataReadError = true;

            if(loadedCollisionDataFiles === numCollisionDataFiles){
                loadedCollisionDataFiles = 0;


                progressCircle.remove();
                progressBar.style.width = 100 + "%";
                progressBar.style.background = params.colorErr;

                //notify
                let message = "Error reading collision data!";
                console.log(message);
                $("#GUIEvents").notify(message, { position:"bottom left", className:"error" });
            }
        }
    };

    particlesFR.addEventListener("progress", eventFilesUpdateProgress_FR);
    particlesFR.addEventListener("load", eventFilesTransferComplete);
    particlesFR.addEventListener("error", eventFilesTransferFailed);
    particlesFR.addEventListener("abort", eventFilesTransferCanceled);
    
    
    //HITS
    var hitsFR = new FileReader();
    hitsFR.readAsText(customEventsFilesList[eventID].hits);
    hitsFR.onload = function(){
        //load complete
        loadedCollisionDataFiles++;


        if (collisionDataReadError === false) {

            var data = hitsFR.result;
            hitsData = JSON.parse(csvToJSON(data));


            if (loadedCollisionDataFiles === numCollisionDataFiles) {
                loadedCollisionDataFiles = 0;


                progressCircle.remove();
                progressBar.style.width = 100 + "%";
                progressBar.style.background = params.colorOK;

                loadCollisionDataV2(particlesData, hitsData, cellsData, truthData);
            }
        }else{
            collisionDataReadError = true;

            if(loadedCollisionDataFiles === numCollisionDataFiles){
                loadedCollisionDataFiles = 0;


                progressCircle.remove();
                progressBar.style.width = 100 + "%";
                progressBar.style.background = params.colorErr;

                //notify
                let message = "Error reading collision data!";
                console.log(message);
                $("#GUIEvents").notify(message, { position:"bottom left", className:"error" });
            }
        }
    };

    hitsFR.addEventListener("progress", eventFilesUpdateProgress_FR);
    hitsFR.addEventListener("load", eventFilesTransferComplete);
    hitsFR.addEventListener("error", eventFilesTransferFailed);
    hitsFR.addEventListener("abort", eventFilesTransferCanceled);
    
    
    //TRUTH
    var truthFR = new FileReader();
    truthFR.readAsText(customEventsFilesList[eventID].truth);
    truthFR.onload = function(){
        //load complete
        loadedCollisionDataFiles++;


        if (collisionDataReadError === false) {

            var data = truthFR.result;
            truthData = JSON.parse(csvToJSON(data));


            if (loadedCollisionDataFiles === numCollisionDataFiles) {
                loadedCollisionDataFiles = 0;


                progressCircle.remove();
                progressBar.style.width = 100 + "%";
                progressBar.style.background = params.colorOK;

                loadCollisionDataV2(particlesData, hitsData, cellsData, truthData);
            }
        }else{
            collisionDataReadError = true;

            if(loadedCollisionDataFiles === numCollisionDataFiles){
                loadedCollisionDataFiles = 0;


                progressCircle.remove();
                progressBar.style.width = 100 + "%";
                progressBar.style.background = params.colorErr;

                //notify
                let message = "Error reading collision data!";
                console.log(message);
                $("#GUIEvents").notify(message, { position:"bottom left", className:"error" });
            }
        }
    };

    truthFR.addEventListener("progress", eventFilesUpdateProgress_FR);
    truthFR.addEventListener("load", eventFilesTransferComplete);
    truthFR.addEventListener("error", eventFilesTransferFailed);
    truthFR.addEventListener("abort", eventFilesTransferCanceled);
}


let particlesObject, hitsObject, tracksObject;
let particleMaterial, hitMaterial, trackMaterial;
var g_Truth;
var compareFolderIcon;
let g_trackIDMap;
function loadCollisionDataV2(particlesData, hitsData, cellsData, truthData) {
    //collision event == one collision of bunched protons near detector center)
    let initial_particles = particlesData;
    let detected_hits = hitsData;
    let cells = cellsData;
    let truth = truthData;
    let tracks = {};
    g_Truth = truth;
    g_trackIDMap = {};

    truth.forEach(function(currentValue, index, arr){
        /*for(var i = index+1; i < truthData.length; i++){
            if(currentValue.particle_id == truthData[i].particle_id){

            }
        }*/

        //if(currentValue.particle_id === 0) return; //add to tracks if its not noise (noise id = 0)
        if(tracks[currentValue.particle_id] === undefined) tracks[currentValue.particle_id] = new Array();
        tracks[currentValue.particle_id].push(currentValue);
    });

    //add initial particle position into tracks
    initial_particles.forEach(function(currentValue, index, arr) {
        var Particle = {
            //hit_id: undefined, //no hit id, its a collision origin
            hit_id: -1,
            particle_id: currentValue.particle_id,
            tpx: currentValue.px,
            tpy: currentValue.py,
            tpz: currentValue.pz,
            tx: currentValue.vx,
            ty: currentValue.vy,
            tz: currentValue.vz
        };

        //if initial particle has a track, add to beginning of an array (function unshift)
        if(tracks[currentValue.particle_id] !== undefined) tracks[currentValue.particle_id].unshift(Particle);
    });


    //TRACK SIZE MANIPULATION
    /*let track_ID_keys = Object.keys(tracks).map(Number);
    let max_track_ID = Number.NEGATIVE_INFINITY;
    for(let i = 0; i < track_ID_keys.length; i++){
        if(track_ID_keys[i] > max_track_ID) max_track_ID = track_ID_keys[i];
    }

    let desired_track_size = 60000;
    let new_tracks = {};
    let padd = 0;
    for(let i = 0; i < desired_track_size; i++){
        if(i >= track_ID_keys.length) padd = max_track_ID + 1;

        new_tracks[i] = tracks[track_ID_keys[i%track_ID_keys.length]];
    }
    tracks = new_tracks;*/


    //calculate length needed later for attribute buffers
    var numTracks = 0;
    var tracksVertexSize = 0;
    var pairedTracksVertexSize = 0;
    /*for(var key in tracks){
        tracksVertexSize += tracks[key].length;
        pairedTracksVertexSize += tracks[key].length + tracks[key].length - 2;
        numTracks++;
    }*/
    let tracksKeys = Object.keys(tracks);
    for(let i = 0; i < tracksKeys.length; i++){
        tracksVertexSize += tracks[tracksKeys[i]].length;
        pairedTracksVertexSize += tracks[tracksKeys[i]].length + tracks[tracksKeys[i]].length - 2;

        g_trackIDMap[tracksKeys[i]] = i;
    }numTracks = tracksKeys.length;


    //event info stats
    console.log("N Particles: " + initial_particles.length);
    console.log("N Hits: " + detected_hits.length);
    console.log("N Tracks: " + Object.keys(tracks).length);


    /*********console.log(tracks["229684405646397440"]);*/


    /********************************************/
    //port gui track data
    trackList = tracks;
    hitList = detected_hits;


    //add gui
    addGUITrackList();
    addGUIHitList();


    //add gui markers
    g_CompareButtonController.name("Compare submission data"); //order important!!

    clearListItemIcon(g_compareEventLI, "iconHolder", "expendable");//te so odvec ane?
    clearListItemProgressIndicator(g_compareEventLI, "loaderHolder", "expendable");
    appendListItemIcon(g_compareEventLI, "iconHolder", "iconFolder", "expendable");


    //flag
    eventLoaded = true;
    /*********************************************/


    //add hit and track list if an event is selected
    /*if(!eventLoaded) {
        eventLoaded = true;
        addGUITrackList();
        addGUIHitList();


        //add submission compare button
        //folderEvents.add(compareButton, "compareSubmissionData").name("Compare submission data");
        //change name of the compare button

        g_CompareButtonController.name("Upload submission data");
    }
    //reset track/hit list
    guiTrackListSection = 0;
    updateGUITrackList(trackList, Object.keys(trackList), folderTracks, guiTrackControllerList);
    guiHitListSection = 0;
    updateGUIHitList(folderHits, guiHitControllerList);*/



    /*if(compareFolderIconHolder === undefined){
        var li = g_CompareButtonController.domElement.parentNode.parentNode;

        compareFolderIconHolder = document.createElement("DIV");
        compareFolderIcon = document.createElement("DIV");

        compareFolderIcon.classList.add("iconFolder");
        compareFolderIconHolder.classList.add("iconHolder");

        compareFolderIconHolder.appendChild(li.childNodes[0]);
        compareFolderIconHolder.appendChild(compareFolderIcon);

        li.appendChild(compareFolderIconHolder);
    }*/






    /*************************************** INITIAL PARTICLES ********************************************/
        //console.log("Particles: " ); console.log(initial_particles);

        //GEOMETRY
    var particlesGeometry = new THREE.BufferGeometry();

    /*var attributes = new Float32Array(initial_particles.length * 7); //will save 7 values from each object of initial_particles
    for(var i = 0; i < initial_particles.length; i++){
        attributes[i*7 + 0] = initial_particles[i].particle_id;
        attributes[i*7 + 1] = initial_particles[i].vx;
        attributes[i*7 + 2] = initial_particles[i].vy;
        attributes[i*7 + 3] = initial_particles[i].vz;
        attributes[i*7 + 4] = initial_particles[i].px;
        attributes[i*7 + 5] = initial_particles[i].py;
        attributes[i*7 + 6] = initial_particles[i].pz;
    }
    particlesGeometry.addAttribute("attributes", new THREE.BufferAttribute(attributes, 7));
    console.log("Particles attributes: "); console.log(attributes);*/

    var position = new Float32Array(initial_particles.length * 3);
    for(var i = 0; i < initial_particles.length; i++){
        position[i*3 + 0] = initial_particles[i].vx;
        position[i*3 + 1] = initial_particles[i].vy;
        position[i*3 + 2] = initial_particles[i].vz;
    }
    particlesGeometry.addAttribute("position", new THREE.BufferAttribute(position, 3));
    //console.log("Particles attributes: "); console.log(position);
    var momentum = new Float32Array(initial_particles.length * 3);
    let momentumVec = new THREE.Vector3();
    /*let maxMomentumNorm = 0;
    for(var i = 0; i < initial_particles.length; i++) {
        momentumVec.x = initial_particles[i].px;
        momentumVec.y = initial_particles[i].py;
        momentumVec.z = initial_particles[i].pz;

        if(momentumVec.length() > maxMomentumNorm) maxMomentumNorm = momentumVec.length();
    }*/
    for(var i = 0; i < initial_particles.length; i++){
        momentumVec.x = initial_particles[i].px;
        momentumVec.y = initial_particles[i].py;
        momentumVec.z = initial_particles[i].pz;
        momentumVec.normalize();
        //momentumVec.divideScalar(maxMomentumNorm);
        momentumVec.multiplyScalar(0.5);
        momentumVec.addScalar(0.5);

        momentum[i*3 + 0] = momentumVec.x;
        momentum[i*3 + 1] = momentumVec.y;
        momentum[i*3 + 2] = momentumVec.z;
    }
    particlesGeometry.addAttribute("momentum", new THREE.BufferAttribute(momentum, 3));
    //console.log("Particles attributes: "); console.log(momentum);
    var particle_id = new Float32Array(initial_particles.length);
    for(var i = 0; i < initial_particles.length; i++){
        //particle_id[i] = initial_particles[i].particle_id;
        particle_id[i] = g_trackIDMap[ initial_particles[i].particle_id ];
    }
    particlesGeometry.addAttribute("particle_id", new THREE.BufferAttribute(particle_id, 1));
    //console.log("Particles attributes: "); console.log(particle_id);


    //MATERIAL
    particleMaterial = new THREE.ShaderMaterial({
        vertexShader: document.getElementById("vertexShaderInitialParticle").textContent,
        fragmentShader: document.getElementById("fragmentShaderInitialParticle").textContent,
        uniforms: {
            selectedTrackVisible: {type: "i", value: +params.visibleSelectedTrack},
            selectedTrackID: {value: params.selectedTrackID},
            particle_size: {value: params.particlesSize},
            particle_color: {type: "v3", value: new THREE.Color(params.particlesColor)},
            particle_alpha: {value: params.opacityParticles},
            particle_color_type: {type: "i", value: params.particlesColorType},
        },
        transparent: true,
        depthTest: false,
        depthWrite: false
    });


    //OBJECT
    particlesObject = new THREE.Points(particlesGeometry, particleMaterial);



    /************************************** DETECTED HITS *************************************************/
    //console.log("Hits: " ); console.log(detected_hits);
    /************let arasf = [];*/

    //GEOMETRY
    var hitsGeometry = new THREE.BufferGeometry();

    var hitsPositions = new Float32Array(detected_hits.length * 3);
    var hitsIDs = new Float32Array(detected_hits.length);
    var hitsParticleID = new Float32Array(detected_hits.length);
    //var submittedHitsParticleID = new Float32Array(detected_hits.length);
    //let el;
    for(var i = 0; i < detected_hits.length; i++){
        hitsPositions[i*3 + 0] = detected_hits[i].x;
        hitsPositions[i*3 + 1] = detected_hits[i].y;
        hitsPositions[i*3 + 2] = detected_hits[i].z;

        hitsIDs[i] = detected_hits[i].hit_id;

        //hitsParticleID[i] = truth[i].particle_id;
        hitsParticleID[i] = g_trackIDMap[ truth[i].particle_id ];
        /**if(truth[i].particle_id === 229684405646397440) arasf.push(hitsParticleID[i]);*/
        /*el = truth.find(function (hit) {
            return hit.hit_id === detected_hits[i].hit_id;
        });
        hitsParticleID[i] = el.particle_id;*/


        //if(params.compareSubmissionData) submittedHitsParticleID[i] = submissionData[i].track_id;
    }
    /**console.log(arasf);*/
    hitsGeometry.addAttribute("position", new THREE.BufferAttribute(hitsPositions, 3));
    //console.log("Hits attributes: "); console.log(hitsPositions);
    hitsGeometry.addAttribute("hit_id", new THREE.BufferAttribute(hitsIDs, 1));
    //console.log("Hits attributes: "); console.log(hitsIDs);
    hitsGeometry.addAttribute("hit_particle_id", new THREE.BufferAttribute(hitsParticleID, 1));
    //console.log("Hits particle ID: "); console.log(hitsParticleID);
    //hitsGeometry.addAttribute("submitted_hit_particle_id", new THREE.BufferAttribute(submittedHitsParticleID, 1));
    ////console.log("Hits particle ID: "); console.log(hitsParticleID);


    /*var hitsInfo = new Float32Array(detected_hits.length * 2);
    for(var i = 0; i < detected_hits.length; i++){
        hitsInfo[i*2 + 0] = detected_hits[i].ncells;
        hitsInfo[i*2 + 1] = detected_hits[i].value;
    }
    hitsGeometry.addAttribute("hit_info", new THREE.BufferAttribute(hitsInfo, 2));*/
    //console.log("Hits attributes: "); console.log(hitsInfo);

    /*var hitsDetectorInfo = new Float32Array(detected_hits.length * 3);
    for(var i = 0; i < detected_hits.length; i++){
        hitsDetectorInfo[i*3 + 0] = detected_hits[i].volume_id;
        hitsDetectorInfo[i*3 + 1] = detected_hits[i].layer_id;
        hitsDetectorInfo[i*3 + 2] = detected_hits[i].module_id;
    }
    hitsGeometry.addAttribute("hit_detector_info", new THREE.BufferAttribute(hitsDetectorInfo, 3));*/
    //console.log("Hits detector attributes: "); console.log(hitsDetectorInfo);


    //MATERIAL
    hitMaterial = new THREE.ShaderMaterial({
        vertexShader: document.getElementById("vertexShaderHit").textContent,
        fragmentShader: document.getElementById("fragmentShaderHit").textContent,
        uniforms: {
            selectedHitVisible: {type: "i", value: +params.visibleSelectedHit},
            selectedHitID: {value: params.selectedHitID},
            selected_hit_size: {value: params.sizeSelectedHit},
            selected_hit_alpha: {value: params.opacitySelectedHit},
            selected_hit_color: {type: "v3", value: new THREE.Color(params.selectedHitColor)},
            selected_hit_color_type: {type: "i", value: params.selectedHitColorType},

            selectedTrackVisible: {type: "i", value: +params.visibleSelectedTrack},
            selectedTrackID: {value: params.selectedTrackID},

            //submissionTesting: {type: "i", value: +params.compareSubmissionData},

            hit_size: {value: params.hitsSize},
            hit_color: {type: "v3", value: new THREE.Color(params.hitsColor)},
            hit_alpha: {value: params.opacityHits},
            hit_color_type: {type: "i", value: params.hitsColorType},

            minMaxBeamPipe: {type: "v2", value: new THREE.Vector2(modelsMinMaxPositions["BeamPipe"].min, modelsMinMaxPositions["BeamPipe"].max)},
            minMaxPix: {type: "v2", value: new THREE.Vector2(modelsMinMaxPositions["Pix"].min, modelsMinMaxPositions["Pix"].max)},
            minMaxPST: {type: "v2", value: new THREE.Vector2(modelsMinMaxPositions["PST"].min, modelsMinMaxPositions["PST"].max)},
            minMaxSStrip: {type: "v2", value: new THREE.Vector2(modelsMinMaxPositions["SStrip"].min, modelsMinMaxPositions["SStrip"].max)},
            minMaxLStrip: {type: "v2", value: new THREE.Vector2(modelsMinMaxPositions["LStrip"].min, modelsMinMaxPositions["LStrip"].max)},

            colorBeamPipe: {type: "v3", value: new THREE.Color(params.colorBeamPipe).add(new THREE.Color(params.ecolorBeamPipe))},
            colorPix: {type: "v3", value: new THREE.Color(params.colorPix).add(new THREE.Color(params.ecolorPix))},
            colorPST: {type: "v3", value: new THREE.Color(params.colorPST).add(new THREE.Color(params.ecolorPST))},
            colorSStrip: {type: "v3", value: new THREE.Color(params.colorSStrip).add(new THREE.Color(params.ecolorSStrip))},
            colorLStrip: {type: "v3", value: new THREE.Color(params.colorLStrip).add(new THREE.Color(params.ecolorLStrip))}
        },
        transparent: true,
        depthTest: false,
        depthWrite: false
    });


    //OBJECT
    hitsObject = new THREE.Points(hitsGeometry, hitMaterial);



    /******************************************* DETECTED TRACKS ******************************************/
    //console.log("Tracks: " ); console.log(tracks);

    //GEOMETRY + DOUBLE
    /*console.log("non indexed");
    var tracksGeometry = new THREE.BufferGeometry();

    var trackPairPositions = new Float32Array(pairedTracksVertexSize * 3);
    var trackPairMomentums = new Float32Array(pairedTracksVertexSize * 3);
    let trackMomentumVec = new THREE.Vector3();
    var tracksIDs = new Float32Array(pairedTracksVertexSize);
    var i = 0;
    var k = 0;
    for(var key in tracks){
        for(var j = 0; j < tracks[key].length; j++) { //preverjeno za pare dela in za ID //a jih uredu concatenata?
            trackPairPositions[i + 0] = tracks[key][j].tx;
            trackPairPositions[i + 1] = tracks[key][j].ty;
            trackPairPositions[i + 2] = tracks[key][j].tz;
            //trackPairMomentums[i + 0] = tracks[key][j].tpx;
            //trackPairMomentums[i + 1] = tracks[key][j].tpy;
            //trackPairMomentums[i + 2] = tracks[key][j].tpz;

            trackMomentumVec.x = tracks[key][j].tpx;
            trackMomentumVec.y = tracks[key][j].tpy;
            trackMomentumVec.z = tracks[key][j].tpz;
            trackMomentumVec.normalize();
            trackMomentumVec.multiplyScalar(0.5);
            trackMomentumVec.addScalar(0.5);
            trackPairMomentums[i + 0] = trackMomentumVec.x;
            trackPairMomentums[i + 1] = trackMomentumVec.y;
            trackPairMomentums[i + 2] = trackMomentumVec.z;

            //tracksIDs[k + 0] = key;
            tracksIDs[k + 0] = g_trackIDMap[ key ];


            if(j > 0 && j < tracks[key].length-1){
                trackPairPositions[i + 3] = tracks[key][j].tx;
                trackPairPositions[i + 4] = tracks[key][j].ty;
                trackPairPositions[i + 5] = tracks[key][j].tz;
                //trackPairMomentums[i + 3] = tracks[key][j].tpx;
                //trackPairMomentums[i + 4] = tracks[key][j].tpy;
                //trackPairMomentums[i + 5] = tracks[key][j].tpz;

                trackMomentumVec.x = tracks[key][j].tpx;
                trackMomentumVec.y = tracks[key][j].tpy;
                trackMomentumVec.z = tracks[key][j].tpz;
                trackMomentumVec.normalize();
                trackMomentumVec.multiplyScalar(0.5);
                trackMomentumVec.addScalar(0.5);
                trackPairMomentums[i + 3] = trackMomentumVec.x;
                trackPairMomentums[i + 4] = trackMomentumVec.y;
                trackPairMomentums[i + 5] = trackMomentumVec.z;

                //tracksIDs[k + 1] = key;
                tracksIDs[k + 1] = g_trackIDMap[ key ];


                i += 3; k++;
            }


            i += 3; k++;
        }
    }

    tracksGeometry.addAttribute("position", new THREE.BufferAttribute(trackPairPositions, 3));
    //console.log("Tracks position: "); console.log(trackPairPositions);

    tracksGeometry.addAttribute("momentum", new THREE.BufferAttribute(trackPairMomentums, 3));
    //console.log("Tracks momentum: "); console.log(trackPairMomentums);

    tracksGeometry.addAttribute("track_id", new THREE.BufferAttribute(tracksIDs, 1));
    //console.log("Tracks ids: "); console.log(tracksIDs);*/

    //GEOMETRY + INDICES
    //console.log("indexed");
    var tracksGeometry = new THREE.BufferGeometry();
    var tracksIndices = [];

    var trackPairPositions = new Float32Array(tracksVertexSize * 3);
    var trackPairMomentums = new Float32Array(tracksVertexSize * 3);
    let trackMomentumVec = new THREE.Vector3();
    var tracksIDs = new Float32Array(tracksVertexSize);
    var i = 0;
    var k = 0;
    for(var key in tracks){
        for(var j = 0; j < tracks[key].length; j++) { //preverjeno za pare dela in za ID //a jih uredu concatenata?
            trackPairPositions[i + 0] = tracks[key][j].tx;
            trackPairPositions[i + 1] = tracks[key][j].ty;
            trackPairPositions[i + 2] = tracks[key][j].tz;

            trackMomentumVec.x = tracks[key][j].tpx;
            trackMomentumVec.y = tracks[key][j].tpy;
            trackMomentumVec.z = tracks[key][j].tpz;
            trackMomentumVec.normalize();
            trackMomentumVec.multiplyScalar(0.5);
            trackMomentumVec.addScalar(0.5);
            trackPairMomentums[i + 0] = trackMomentumVec.x;
            trackPairMomentums[i + 1] = trackMomentumVec.y;
            trackPairMomentums[i + 2] = trackMomentumVec.z;

            //tracksIDs[k + 0] = key;
            tracksIDs[k + 0] = g_trackIDMap[ key ];

            tracksIndices.push(k);

            if(j > 0 && j < tracks[key].length-1){
                tracksIndices.push(k);
            }


            i += 3; k += 1;
        }
    }

    tracksGeometry.setIndex(tracksIndices);
    //console.log("Tracks indices: "); console.log(tracksIndices);

    tracksGeometry.addAttribute("position", new THREE.BufferAttribute(trackPairPositions, 3));
    //console.log("Tracks position: "); console.log(trackPairPositions);

    tracksGeometry.addAttribute("momentum", new THREE.BufferAttribute(trackPairMomentums, 3));
    //console.log("Tracks momentum: "); console.log(trackPairMomentums);

    tracksGeometry.addAttribute("track_id", new THREE.BufferAttribute(tracksIDs, 1));
    //console.log("Tracks ids: "); console.log(tracksIDs);


    //MATERIAL
    trackMaterial = new THREE.ShaderMaterial({
        vertexShader: document.getElementById("vertexShaderTrack").textContent,
        fragmentShader: document.getElementById("fragmentShaderTrack").textContent,
        uniforms: {
            selectedTrackVisible: {type: "i", value: +params.visibleSelectedTrack},
            selectedTrackID: {value: params.selectedTrackID}, //selectedTrackID: {value: null}
            numTracks: {value: Object.keys(tracks).length},
            track_size: {value: params.tracksSize},
            track_color: {type: "v3", value: new THREE.Color(params.tracksColor)},
            track_alpha: {value: params.opacityTracks},
            track_color_type: {type: "i", value: params.tracksColorType},

            minMaxBeamPipe: {type: "v2", value: new THREE.Vector2(modelsMinMaxPositions["BeamPipe"].min, modelsMinMaxPositions["BeamPipe"].max)},
            minMaxPix: {type: "v2", value: new THREE.Vector2(modelsMinMaxPositions["Pix"].min, modelsMinMaxPositions["Pix"].max)},
            minMaxPST: {type: "v2", value: new THREE.Vector2(modelsMinMaxPositions["PST"].min, modelsMinMaxPositions["PST"].max)},
            minMaxSStrip: {type: "v2", value: new THREE.Vector2(modelsMinMaxPositions["SStrip"].min, modelsMinMaxPositions["SStrip"].max)},
            minMaxLStrip: {type: "v2", value: new THREE.Vector2(modelsMinMaxPositions["LStrip"].min, modelsMinMaxPositions["LStrip"].max)},

            colorBeamPipe: {type: "v3", value: new THREE.Color(params.colorBeamPipe).add(new THREE.Color(params.ecolorBeamPipe))},
            colorPix: {type: "v3", value: new THREE.Color(params.colorPix).add(new THREE.Color(params.ecolorPix))},
            colorPST: {type: "v3", value: new THREE.Color(params.colorPST).add(new THREE.Color(params.ecolorPST))},
            colorSStrip: {type: "v3", value: new THREE.Color(params.colorSStrip).add(new THREE.Color(params.ecolorSStrip))},
            colorLStrip: {type: "v3", value: new THREE.Color(params.colorLStrip).add(new THREE.Color(params.ecolorLStrip))}
        },
        /*transparent: false,
        depthTest: true,
        depthWrite: true*/
        transparent: true,
        depthTest: false,
        depthWrite: false
    });


    //OBJECT
    tracksObject = new THREE.LineSegments(tracksGeometry, trackMaterial);



    //ADD TO SCENE
    particlesObject.visible = params.visibleParticles;
    hitsObject.visible = params.visibleHits;
    tracksObject.visible = params.visibleTracks;

    particlesObject.name = "Particles";
    hitsObject.name = "Hits";
    tracksObject.name = "Tracks";

    //remove any previous objects
    /*scene.remove(scene.getObjectByName("Particles"));
    scene.remove(scene.getObjectByName("Hits"));
    scene.remove(scene.getObjectByName("Tracks"));
    scene.remove(scene.getObjectByName("SelectedTrack"));
    scene.remove(scene.getObjectByName("SubmissionTracks"));
    scene.remove(scene.getObjectByName("SelectedSubmissionTrack"));*/

    particlesObject.renderOrder = params.particlesRenderOrder;
    hitsObject.renderOrder = params.hitsRenderOrder;
    tracksObject.renderOrder = params.tracksRenderOrder;

    scene.add(particlesObject);
    scene.add(hitsObject);
    scene.add(tracksObject);

}
function removeEntity(object) {
    var selectedObject = scene.getObjectByName(object.name);
    scene.remove(selectedObject);
}