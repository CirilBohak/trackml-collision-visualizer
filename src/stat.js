let stat = {};


stat.enableTesting = false;
stat.data = [];
stat.sampleSize = 0;
stat.sampleIndex = 0;
stat.indicatorLI = null;
stat.progressController = null;


stat.initTest = function initTest(sampleSize, indicatorLI){
    stat.enableTesting = true;
    //stat.data = [];
    stat.data = new Array(sampleSize);
    stat.sampleSize = sampleSize;
    stat.sampleIndex = 0;


    stat.indicatorLI = indicatorLI;
    clearListItemProgressIndicator(stat.indicatorLI, "loaderHolder", "expendable");
    stat.progressController = appendListItemProgressIndicator(stat.indicatorLI, "loaderHolder", "loaderBar", "loaderCircle", "expendable");
    //console.log(progressController);
    stat.progressController.progressBar.style.width = stat.sampleIndex/stat.sampleSize*100 + "%";
};

stat.test = function(deltaTime){
    if(stat.enableTesting){
        //stat.data.push(deltaTime);
        stat.data[stat.sampleIndex] = deltaTime;
        stat.sampleIndex++;


        if(stat.sampleIndex % 100 === 0)
            stat.progressController.progressBar.style.width = stat.sampleIndex/stat.sampleSize*100 + "%";


        //if(stat.data.length >= stat.sampleSize) {
        if(stat.sampleIndex >= stat.sampleSize) {
            stat.enableTesting = false;

            let avg = stat.average(stat.data);
            let std = stat.stdDev(stat.data);
            //console.log("FPS:" + 1/avg + ", " + "Frame Time: " + avg);
            //console.log("FPS:" + 1/std + ", " + "Frame Time: " + std);
            clearListItemProgressIndicator(stat.indicatorLI, "loaderHolder", "expendable");
            console.log("Frame time: " + "Mean: " + avg + ", " + "Std: " + std);

            /*for(let i = 0; i < stat.data.length; i++){
                stat.data[i] = 1/stat.data[i];
            }
            avg = stat.average(stat.data);
            std = stat.stdDev(stat.data);
            console.log("FPS: " + "Mean: " + avg + ", " + "Std: " + std);*/
        }
    }
};

stat.average = function(data, N){
    if(N === undefined) N = data.length;

    let sum = data.reduce(function(sum, value){
        return sum + value;
    }, 0);

    let avg = sum / N;

    return avg;
};


stat.stdDev = function(data){
    let avg = stat.average(data, data.lenght);

    let squareDiffs = data.map(function(value){
        let diff = value - avg;
        let sqrDiff = diff * diff;

        return sqrDiff;
    });


    let avgSquareDiff = stat.average(squareDiffs, squareDiffs.length-1);
    let stdDev = Math.sqrt(avgSquareDiff);

    return stdDev;
};
