function detectorsFileUpdateProgress (oEvent) {
    if (oEvent.lengthComputable) {

    } else {
        // Unable to compute progress information since the total size is unknown
    }
}

function detectorsFileTransferComplete(evt) {
    transferComplete(evt);
}

function detectorsFileTransferFailed(evt) {
    console.log("Error reading detectors file!");
}

function detectorsFileTransferCanceled(evt) {
    transferCanceled(evt);
}


var planeX = new THREE.Plane(new THREE.Vector3(1, 0, 0), -params.XYcutPlaneOffset);
var planeY = new THREE.Plane(new THREE.Vector3(0, -1, 0), -params.XYcutPlaneOffset);
var planeZ = new THREE.Plane(new THREE.Vector3(0, 0, -1), -params.ZcutPlaneOffset);

var materialBeamPipe = new THREE.MeshLambertMaterial( {
    color: params.colorBeamPipe,
    emissive: params.ecolorBeamPipe,
    //side: THREE.DoubleSide,
    opacity: params.opacityBeamPipe,
    transparent: true,
    depthTest: true,
    depthWrite: true,
    clippingPlanes: [planeX, planeY, planeZ],
    clipIntersection: true
});
var materialPix = new THREE.MeshLambertMaterial( {
    color: params.colorPix,
    emissive: params.ecolorPix,
    //side: THREE.DoubleSide,
    opacity: params.opacityPix,
    transparent: true,
    depthTest: true,
    depthWrite: true,
    //wireframe: true,
    //wireframeLinewidth: 0.5
    clippingPlanes: [planeX, planeY, planeZ],
    clipIntersection: true
});
var materialPST = new THREE.MeshLambertMaterial( {
    color: params.colorPST,
    emissive: params.ecolorPST,
    //side: THREE.DoubleSide,
    opacity: params.opacityPST,
    transparent: true,
    depthTest: true,
    depthWrite: true,
    clippingPlanes: [planeX, planeY, planeZ],
    clipIntersection: true
});
var materialSStrip = new THREE.MeshLambertMaterial( {
    color: params.colorSStrip,
    emissive: params.ecolorSStrip,
    //side: THREE.DoubleSide,
    opacity: params.opacitySStrip,
    transparent: true,
    depthTest: true,
    depthWrite: true,
    clippingPlanes: [planeX, planeY, planeZ],
    clipIntersection: true
});
var materialLStrip = new THREE.MeshLambertMaterial( {
    color: params.colorLStrip,
    emissive: params.ecolorLStrip,
    //side: THREE.DoubleSide,
    opacity: params.opacityLStrip,
    transparent: true,
    depthTest: true,
    depthWrite: true,
    clippingPlanes: [planeX, planeY, planeZ],
    clipIntersection: true
});
var materialBeamPipeB = materialBeamPipe.clone();
var materialPixB = materialPix.clone();
var materialPSTB = materialPST.clone();
var materialSStripB = materialSStrip.clone();
var materialLStripB = materialLStrip.clone();
materialBeamPipeB.depthTest = true;
materialBeamPipeB.depthWrite = true;
materialPixB.depthTest = true;
materialPixB.depthWrite = true;
materialPSTB.depthTest = true;
materialPSTB.depthWrite = true;
materialSStripB.depthTest = true;
materialSStripB.depthWrite = true;
materialLStripB.depthTest = true;
materialLStripB.depthWrite = true;
materialBeamPipeB.clippingPlanes = [planeX, planeY, planeZ];
materialPixB.clippingPlanes = [planeX, planeY, planeZ];
materialPSTB.clippingPlanes = [planeX, planeY, planeZ];
materialSStripB.clippingPlanes = [planeX, planeY, planeZ];
materialLStripB.clippingPlanes = [planeX, planeY, planeZ];



//var detectorsData; moved to local var
//var pixObjectFNew, pixObjectBNew, sStripObjectFNew, sStripObjectBNew, lStripObjectFNew, lStripObjectBNew; renamed to existing var


function readDetectorsFile(){
    var detectorsReq = new XMLHttpRequest();
    detectorsReq.onload = function(e) {
        if (this.readyState === 4 && this.status === 200) {


            var data = detectorsReq.response;
            let detectorsData = JSON.parse(csvToJSON(data));


            //var selectedDetectorData = selectDetectorsData(detectorsData, 6); // 1 - 5, 6 experimental
            var segmentedDetectorData = selectDetectorsData(detectorsData);


            for (var i = 0; i < segmentedDetectorData.length; i++) {
                loadDetectorsData(segmentedDetectorData[i], i);
            }
            //combined geometry
            //loadDetectorsDataV2(segmentedDetectorData);
        }else{
            console.log("Error reading detectors file: " + this.status);
        }
    };

    detectorsReq.addEventListener("progress", null);
    detectorsReq.addEventListener("load", detectorsFileTransferComplete);
    detectorsReq.addEventListener("error", detectorsFileTransferFailed);
    detectorsReq.addEventListener("abort", detectorsFileTransferCanceled);

    detectorsReq.open("GET", "./data/csv/detectors.csv", true);
    detectorsReq.send();
}


function selectDetectorsData(detectorsData, detector){
    var selectedDetectorsData = [];
    var interestVolumes;


    switch (detector) {
        case 1:
            interestVolumes = ['7', '8', '9'];
            break;
        case 2:
            interestVolumes = ["12", "13", "14"];
            break;
        case 3:
            interestVolumes = ["16"];
            break;
        case 4:
            interestVolumes = ["17"];
            break;
        case 5:
            interestVolumes = ["18"];
            break;
		case 6:
            interestVolumes = ["16", "17", "18"];
    }


    for(var i = 0; i < detectorsData.length; i++){
        for(var j = 0; j < interestVolumes.length; j++){
            if(detectorsData[i].volume_id === interestVolumes[j]) selectedDetectorsData.push(detectorsData[i]);
		}
    }


    return selectedDetectorsData;
}


function selectDetectorsData(detectorsData){
    var detectorsVolumeSegments = [['7', '8', '9'], ["12", "13", "14"], ["16", "17", "18"]];
    var segmentedDetectorsData = [[], [], []];


    for(var i = 0; i < detectorsData.length; i++){
        for(var j = 0; j < detectorsVolumeSegments.length; j++){

            for(var k = 0; k < detectorsVolumeSegments[j].length; k++) {
                if (detectorsData[i].volume_id === detectorsVolumeSegments[j][k]) segmentedDetectorsData[j].push(detectorsData[i]);
            }

        }
    }


    return segmentedDetectorsData;
}


function loadDetectorsData(detectorsData, detectorIndex) {
    /*************************************** DETECTORS ********************************************/
    //console.log("Detectors: " ); console.log(detectorsData);
    // GEOMETRY
    var detectorsGeometry = new THREE.BufferGeometry();
    //var G = new THREE.Geometry();


    var position = new Float32Array(detectorsData.length * 8 * 3); //8 vertices per single detector center, 3 components(x, y, z)
    var index = [];

    var pos_xyz = new THREE.Vector3();
    var rotation_matrix = new THREE.Matrix3();
    var pos_uvw = new THREE.Vector3();
    var translation = new THREE.Vector3();


    for(var i = 0; i < detectorsData.length; i++){
        //for every detector module:

        rotation_matrix.set(detectorsData[i].rot_xu, detectorsData[i].rot_xv, detectorsData[i].rot_xw,
                            detectorsData[i].rot_yu, detectorsData[i].rot_yv, detectorsData[i].rot_yw,
                            detectorsData[i].rot_zu, detectorsData[i].rot_zv, detectorsData[i].rot_zw);

        translation.x = detectorsData[i].cx;
        translation.y = detectorsData[i].cy;
        translation.z = detectorsData[i].cz;


        pos_uvw.x = -detectorsData[i].module_minhu;
        pos_uvw.y = -detectorsData[i].module_hv;
        pos_uvw.z = +detectorsData[i].module_t;

        pos_xyz = pos_uvw.applyMatrix3(rotation_matrix).add(translation);

        position[i*8*3 +  0] = pos_xyz.x;
        position[i*8*3 +  1] = pos_xyz.y;
        position[i*8*3 +  2] = pos_xyz.z;
        //G.vertices.push(new THREE.Vector3(pos_xyz.x, pos_xyz.y, pos_xyz.z) );


        pos_uvw.x = -detectorsData[i].module_maxhu;
        pos_uvw.y = +detectorsData[i].module_hv;
        pos_uvw.z = +detectorsData[i].module_t;

        pos_xyz = pos_uvw.applyMatrix3(rotation_matrix).add(translation);

        position[i*8*3 +  3] = pos_xyz.x;
        position[i*8*3 +  4] = pos_xyz.y;
        position[i*8*3 +  5] = pos_xyz.z;
        //G.vertices.push(new THREE.Vector3(pos_xyz.x, pos_xyz.y, pos_xyz.z) );


        pos_uvw.x = +detectorsData[i].module_minhu;
        pos_uvw.y = -detectorsData[i].module_hv;
        pos_uvw.z = +detectorsData[i].module_t;

        pos_xyz = pos_uvw.applyMatrix3(rotation_matrix).add(translation);

        position[i*8*3 +  6] = pos_xyz.x;
        position[i*8*3 +  7] = pos_xyz.y;
        position[i*8*3 +  8] = pos_xyz.z;
        //G.vertices.push(new THREE.Vector3(pos_xyz.x, pos_xyz.y, pos_xyz.z) );


        pos_uvw.x = +detectorsData[i].module_maxhu;
        pos_uvw.y = +detectorsData[i].module_hv;
        pos_uvw.z = +detectorsData[i].module_t;

        pos_xyz = pos_uvw.applyMatrix3(rotation_matrix).add(translation);

        position[i*8*3 +  9] = pos_xyz.x;
        position[i*8*3 + 10] = pos_xyz.y;
        position[i*8*3 + 11] = pos_xyz.z;
        //G.vertices.push(new THREE.Vector3(pos_xyz.x, pos_xyz.y, pos_xyz.z) );


        pos_uvw.x = -detectorsData[i].module_minhu;
        pos_uvw.y = -detectorsData[i].module_hv;
        pos_uvw.z = -detectorsData[i].module_t;

        pos_xyz = pos_uvw.applyMatrix3(rotation_matrix).add(translation);

        position[i*8*3 +  12] = pos_xyz.x;
        position[i*8*3 +  13] = pos_xyz.y;
        position[i*8*3 +  14] = pos_xyz.z;
        //G.vertices.push(new THREE.Vector3(pos_xyz.x, pos_xyz.y, pos_xyz.z) );


        pos_uvw.x = -detectorsData[i].module_maxhu;
        pos_uvw.y = +detectorsData[i].module_hv;
        pos_uvw.z = -detectorsData[i].module_t;

        pos_xyz = pos_uvw.applyMatrix3(rotation_matrix).add(translation);

        position[i*8*3 +  15] = pos_xyz.x;
        position[i*8*3 +  16] = pos_xyz.y;
        position[i*8*3 +  17] = pos_xyz.z;
        //G.vertices.push(new THREE.Vector3(pos_xyz.x, pos_xyz.y, pos_xyz.z) );


        pos_uvw.x = +detectorsData[i].module_minhu;
        pos_uvw.y = -detectorsData[i].module_hv;
        pos_uvw.z = -detectorsData[i].module_t;

        pos_xyz = pos_uvw.applyMatrix3(rotation_matrix).add(translation);

        position[i*8*3 +  18] = pos_xyz.x;
        position[i*8*3 +  19] = pos_xyz.y;
        position[i*8*3 +  20] = pos_xyz.z;
        //G.vertices.push(new THREE.Vector3(pos_xyz.x, pos_xyz.y, pos_xyz.z) );


        pos_uvw.x = +detectorsData[i].module_maxhu;
        pos_uvw.y = +detectorsData[i].module_hv;
        pos_uvw.z = -detectorsData[i].module_t;

        pos_xyz = pos_uvw.applyMatrix3(rotation_matrix).add(translation);

        position[i*8*3 + 21] = pos_xyz.x;
        position[i*8*3 + 22] = pos_xyz.y;
        position[i*8*3 + 23] = pos_xyz.z;
        //G.vertices.push(new THREE.Vector3(pos_xyz.x, pos_xyz.y, pos_xyz.z) );


        index.push(i*8+0, i*8+3, i*8+1, i*8+0, i*8+2, i*8+3); //front
        index.push(i*8+2, i*8+7, i*8+3, i*8+2, i*8+6, i*8+7); //right
        index.push(i*8+6, i*8+5, i*8+7, i*8+6, i*8+4, i*8+5); //back
        index.push(i*8+4, i*8+1, i*8+5, i*8+4, i*8+0, i*8+1); //left
        index.push(i*8+1, i*8+7, i*8+5, i*8+1, i*8+3, i*8+7); //up
        index.push(i*8+4, i*8+2, i*8+0, i*8+4, i*8+6, i*8+2); //down
        /*index.push(i*8+0, i*8+1, i*8+3, i*8+0, i*8+3, i*8+2); //front
        index.push(i*8+2, i*8+3, i*8+7, i*8+2, i*8+7, i*8+6); //right
        index.push(i*8+6, i*8+7, i*8+5, i*8+6, i*8+5, i*8+4); //back
        index.push(i*8+4, i*8+5, i*8+1, i*8+4, i*8+1, i*8+0); //left
        index.push(i*8+1, i*8+5, i*8+7, i*8+1, i*8+7, i*8+3); //up
        index.push(i*8+4, i*8+0, i*8+2, i*8+4, i*8+2, i*8+6); //down*/

        /*G.faces.push( new THREE.Face3( i*8+0, i*8+3, i*8+1) );
        G.faces.push( new THREE.Face3( i*8+0, i*8+2, i*8+3) );
        G.faces.push( new THREE.Face3( i*8+2, i*8+7, i*8+3) );
        G.faces.push( new THREE.Face3( i*8+2, i*8+6, i*8+7) );
        G.faces.push( new THREE.Face3( i*8+6, i*8+5, i*8+7) );
        G.faces.push( new THREE.Face3( i*8+6, i*8+4, i*8+5) );

        G.faces.push( new THREE.Face3( i*8+4, i*8+1, i*8+5) );
        G.faces.push( new THREE.Face3( i*8+4, i*8+0, i*8+1) );
        G.faces.push( new THREE.Face3( i*8+1, i*8+7, i*8+5) );
        G.faces.push( new THREE.Face3( i*8+1, i*8+3, i*8+7) );
        G.faces.push( new THREE.Face3( i*8+4, i*8+2, i*8+0) );
        G.faces.push( new THREE.Face3( i*8+4, i*8+6, i*8+2) );*/
        /*G.faces.push( new THREE.Face3( i*8+0,  i*8+1, i*8+3) );
        G.faces.push( new THREE.Face3( i*8+0,  i*8+3, i*8+2) );
        G.faces.push( new THREE.Face3( i*8+2,  i*8+3, i*8+7) );
        G.faces.push( new THREE.Face3( i*8+2,  i*8+7, i*8+6) );
        G.faces.push( new THREE.Face3( i*8+6,  i*8+7, i*8+5) );
        G.faces.push( new THREE.Face3( i*8+6,  i*8+5, i*8+4) );

        G.faces.push( new THREE.Face3( i*8+4,  i*8+5, i*8+1) );
        G.faces.push( new THREE.Face3( i*8+4,  i*8+1, i*8+0) );
        G.faces.push( new THREE.Face3( i*8+1,  i*8+5, i*8+7) );
        G.faces.push( new THREE.Face3( i*8+1,  i*8+7, i*8+3) );
        G.faces.push( new THREE.Face3( i*8+4,  i*8+0, i*8+2) );
        G.faces.push( new THREE.Face3( i*8+4,  i*8+2, i*8+6) );*/
    }
    detectorsGeometry.addAttribute("position", new THREE.BufferAttribute(position, 3));
    //console.log("Detectors attributes: "); console.log(position);

    detectorsGeometry.setIndex(index);
    //console.log("Detectors indices: "); console.log(index);


    //detectorsGeometry.computeFaceNormals(); //not in buffer geometry
    detectorsGeometry.computeVertexNormals();

    //G.computeFaceNormals();
    //G.computeVertexNormals();


    //MATERIAL
    var detectorsMaterial = new THREE.MeshLambertMaterial( {
        color: 0x991111,
        emissive: 0x111199,
        opacity: 0.16,
        side: THREE.DoubleSide,
        transparent: true,
        depthTest: false,
        depthWrite: false,
    });


    //OBJECT
    var detectorsObject = new THREE.Mesh(detectorsGeometry, detectorsMaterial);
    //var detectorsObject = new THREE.Mesh(G, detectorsMaterial);
    detectorsObject.drawMode = THREE.TrianglesDrawMode; // default



    //ADD TO SCENE
    //scene.add(detectorsObject);



    //ASSIGN OBJECTS
    switch (detectorIndex) {
        case 0:
            Pix = detectorsObject;
            break;
        case 1:
            SStrip = detectorsObject;
            break;
        case 2:
            LStrip = detectorsObject;
            loadModels();
            break;
    }
}


let combinedDetectorsObject;
let combinedDetectorsMaterial;
function loadDetectorsDataV2(segmentedDetectorData) {
    for(let i = 0; i < segmentedDetectorData.length; i++){
        loadDetectorsData(segmentedDetectorData[i], i);
    }


    
    /*************************************** COMBINED DETECTORS ********************************************/
    //console.log("Combined detectors: " ); console.log(detectorsData);
    // GEOMETRY
    let combinedDetectorsGeometry = new THREE.BufferGeometry();


    let totalVertexSize = Pix.geometry.attributes.position.array.length + SStrip.geometry.attributes.position.array.length + LStrip.geometry.attributes.position.array.length;
    let totalVertexCount = Pix.geometry.attributes.position.count + SStrip.geometry.attributes.position.count + LStrip.geometry.attributes.position.count;
    let totalIndexSize = Pix.geometry.index.array.length + SStrip.geometry.index.array.length + LStrip.geometry.index.array.length;
    let totalNormalSize = Pix.geometry.attributes.normal.array.length + SStrip.geometry.attributes.normal.array.length + LStrip.geometry.attributes.normal.array.length;

    let position = new Float32Array(totalVertexSize);
    var detectorID = new Float32Array(totalVertexCount);
    //let index = new Uint16Array(totalIndexSize); //do not use, does not work with geometry.setIndex() func
    let index = [];
    let normal = new Float32Array(totalNormalSize);


    copyArray(position, Pix.geometry.attributes.position.array, 0, Pix.geometry.attributes.position.array.length);
    copyArray(position, SStrip.geometry.attributes.position.array, Pix.geometry.attributes.position.array.length, SStrip.geometry.attributes.position.array.length);
    copyArray(position, LStrip.geometry.attributes.position.array, (Pix.geometry.attributes.position.array.length + SStrip.geometry.attributes.position.array.length), LStrip.geometry.attributes.position.array.length);

    copyValue(detectorID, 0, 0, Pix.geometry.attributes.position.count);
    copyValue(detectorID, 1, Pix.geometry.attributes.position.count, SStrip.geometry.attributes.position.count);
    copyValue(detectorID, 2, (Pix.geometry.attributes.position.count + SStrip.geometry.attributes.position.count), LStrip.geometry.attributes.position.count);


    /*copyArray(index, Pix.geometry.index.array, 0, Pix.geometry.index.array.length);
    copyArray(index, SStrip.geometry.index.array, Pix.geometry.index.array.length, SStrip.geometry.index.array.length);
    copyArray(index, LStrip.geometry.index.array, (Pix.geometry.index.array.length + SStrip.geometry.index.array.length), LStrip.geometry.index.array.length);*/

    for(let i = 0; i < Pix.geometry.index.array.length; i++){
        index[i] = Pix.geometry.index.array[i];
    }
    for(let i = 0; i < SStrip.geometry.index.array.length; i++){
        index[i+Pix.geometry.index.array.length] = SStrip.geometry.index.array[i] + Pix.geometry.attributes.position.count;
    }
    for(let i = 0; i < LStrip.geometry.index.array.length; i++){
        index[i+Pix.geometry.index.array.length+SStrip.geometry.index.array.length] = LStrip.geometry.index.array[i] + Pix.geometry.attributes.position.count + SStrip.geometry.attributes.position.count;
    }
    //add additional index combination
    for(let i = 0; i < Pix.geometry.index.array.length; i++){
        index[i+Pix.geometry.index.array.length+SStrip.geometry.index.array.length+LStrip.geometry.index.array.length] = Pix.geometry.index.array[i];
    }



    copyArray(normal, Pix.geometry.attributes.normal.array, 0, Pix.geometry.attributes.normal.array.length);
    copyArray(normal, SStrip.geometry.attributes.normal.array, Pix.geometry.attributes.normal.array.length, SStrip.geometry.attributes.normal.array.length);
    copyArray(normal, LStrip.geometry.attributes.normal.array, (Pix.geometry.attributes.normal.array.length + SStrip.geometry.attributes.normal.array.length), LStrip.geometry.attributes.normal.array.length);


    combinedDetectorsGeometry.addAttribute("position", new THREE.BufferAttribute(position, 3));
    //console.log("Detectors attributes: "); console.log(position);

    combinedDetectorsGeometry.addAttribute("detectorID", new THREE.BufferAttribute(detectorID, 1));
    //console.log("Detectors attributes: "); console.log(detectorID);

    combinedDetectorsGeometry.setIndex(index);
    //console.log("Detectors indices: "); console.log(index);

    combinedDetectorsGeometry.addAttribute("normal", new THREE.BufferAttribute(normal, 3));
    //console.log("Detectors attributes: "); console.log(normal);


    //draw range examples
    //combinedDetectorsGeometry.setDrawRange(0, Pix.geometry.index.array.length);
    //combinedDetectorsGeometry.setDrawRange(Pix.geometry.index.array.length, SStrip.geometry.index.array.length);
    //combinedDetectorsGeometry.setDrawRange(Pix.geometry.index.array.length+SStrip.geometry.index.array.length, LStrip.geometry.index.array.length);

    //combinedDetectorsGeometry.setDrawRange(Pix.geometry.index.array.length, SStrip.geometry.index.array.length+LStrip.geometry.index.array.length);
    //combinedDetectorsGeometry.setDrawRange(Pix.geometry.index.array.length+SStrip.geometry.index.array.length, LStrip.geometry.index.array.length+Pix.geometry.index.array.length);


    //MATERIAL
    let combinedDetectorsMaterialDEMO = new THREE.MeshLambertMaterial( {
        color: 0x991111,
        emissive: 0x111199,
        opacity: 0.16,
        side: THREE.DoubleSide,
        transparent: true,
        depthTest: false,
        depthWrite: false,
    });


    let lightPositions = [];
    let lightIntensities = [];
    let lightColors = [];
    for(let i = 0; i < lights.length; i++){
        lightPositions.push(lights[i].position);
        lightIntensities.push(lights[i].intensity);
        lightColors.push(lights[i].color);
    }

    //[T1, T2, T3]
    let detectorTypeAlphas = [params.opacityPix, params.opacitySStrip, params.opacityLStrip];
    let detectorTypeColors = [new THREE.Color(params.colorPix), new THREE.Color(params.colorSStrip), new THREE.Color(params.colorLStrip)];
    let detectorTypeEColors = [new THREE.Color(params.ecolorPix), new THREE.Color(params.ecolorSStrip), new THREE.Color(params.ecolorLStrip)];


    combinedDetectorsMaterial = new THREE.ShaderMaterial({
        vertexShader: document.getElementById("vertexShaderCombinedDetectors").textContent,
        fragmentShader: document.getElementById("fragmentShaderCombinedDetectors").textContent,
        uniforms: {
            light_positions: {type: "v3v", value: lightPositions},
            light_intensities: {type: "iv1", value: lightIntensities},
            light_colors: {type: "v3v", value: lightColors},
            detector_type_alphas: {type: "fv1", value: detectorTypeAlphas},
            detector_type_colors: {type: "v3v", value: detectorTypeColors},
            detector_type_ecolors: {type: "v3v", value: detectorTypeEColors}
        },
        transparent: true,
        depthTest: false,
        depthWrite: false,
        side: THREE.DoubleSide,
        clippingPlanes: [params.planeX, params.planeY, params.planeZ],
        clipIntersection: true
    });



    //OBJECT
    combinedDetectorsObject = new THREE.Mesh(combinedDetectorsGeometry, combinedDetectorsMaterial);
    combinedDetectorsObject.drawMode = THREE.TrianglesDrawMode; // default



    //ADD TO SCENE
    //scene.add(combinedDetectorsObject);


    loadModels();
}

// load collider models
var BeamPipe, Pix, PST, SStrip, LStrip;
var BeamPipeB, PixB, PSTB, SStripB, LStripB;
var modelsMinMaxPositions = {};
function loadModels() {
    // instantiate a loader
    var loader = new THREE.OBJLoader();


    loader.load( // BeamPipe
        './data/obj/BeamPipe.obj',
        function ( object ) {
            BeamPipe = object;
            BeamPipeB = BeamPipe.clone();

            object.traverse( function ( child ) {
                if ( child instanceof THREE.Mesh ) {
                    child.material = materialBeamPipe;
                    child.material.side = THREE.BackSide;
                    child.material.depthTest = false;
                    child.renderOrder = params.beamPipeFRenderOrder;
                }
            } );
            scene.add( object );
            modelsMinMaxPositions["BeamPipe"] = getObjectMinMaxPosition(object);

            BeamPipeB.traverse( function ( child ) {
                if ( child instanceof THREE.Mesh ) {
                    child.material = materialBeamPipeB;
                    child.material.side = THREE.FrontSide;
                    //child.material.depthTest = false;
                    child.renderOrder = params.beamPipeBRenderOrder;
                }
            } );
            scene.add( BeamPipeB );

        },
        xhr => console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' ),
        xhr => console.log( 'An error happened' )
    );

    /*loader.load( // Pix
        './data/obj/Pix.obj',
        function ( object ) {
            Pix = Pix;
            PixB = Pix.clone();

            Pix.traverse( function ( child ) {
                if ( child instanceof THREE.Mesh ) {
                    child.material = materialPix;
                    child.material.side = THREE.FrontSide;
                    //child.material.depthTest = false;
                    child.renderOrder = 7;
                }
            } );
            scene.add( Pix );
            modelsMinMaxPositions["Pix"] = getObjectMinMaxPosition(Pix);

            PixB.traverse( function ( child ) {
                if ( child instanceof THREE.Mesh ) {
                    child.material = materialPixB;
                    child.material.side = THREE.BackSide;
                    child.material.depthTest = false;
                    child.renderOrder = 4;
                }
            } );
            scene.add( PixB );
            },
        xhr => console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' ),
        xhr => console.log( 'An error happened' )
    );*/

    loader.load( // PST
        './data/obj/PST.obj',
        function ( object ) {
            PST = object;
            PSTB = object.clone();

            object.traverse( function ( child ) {
                if ( child instanceof THREE.Mesh ) {
                    child.material = materialPST;
                    child.material.side = THREE.BackSide;
                    child.material.depthTest = false;
                    child.renderOrder = params.PSTFRenderOrder;
                }
            } );
            scene.add( object );
            modelsMinMaxPositions["PST"] = getObjectMinMaxPosition(object);

            PSTB.traverse( function ( child ) {
                if ( child instanceof THREE.Mesh ) {
                    child.material = materialPSTB;
                    child.material.side = THREE.FrontSide;
                    //child.material.depthTest = false;
                    child.renderOrder = params.PSTBRenderOrder;
                }
            } );
            scene.add( PSTB );

        },
        xhr => console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' ),
        xhr => console.log( 'An error happened' )
    );

    /*loader.load( // SStrip
        './data/obj/SStrip.obj',
        function ( object ) {
            SStrip = SStrip;
            SStripB = SStrip.clone();

            SStrip.traverse( function ( child ) {
                if ( child instanceof THREE.Mesh ) {
                    child.material = materialSStrip;
                    child.material.side = THREE.FrontSide;
                    //child.material.depthTest = false;
                    child.renderOrder = 9;
                }
            } );
            scene.add( SStrip );
            modelsMinMaxPositions["SStrip"] = getObjectMinMaxPosition(SStrip);

            SStripB.traverse( function ( child ) {
                if ( child instanceof THREE.Mesh ) {
                    child.material = materialSStripB;
                    child.material.side = THREE.BackSide;
                    child.material.depthTest = false;
                    child.renderOrder = 2;
                }
            } );
            scene.add( SStripB );

            },
        xhr => console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' ),
        xhr => console.log( 'An error happened' )
    );*/

    /*loader.load( // LStrip
        './data/obj/LStrip.obj',
        function ( object ) {
            LStrip = LStrip;
            LStripB = LStrip.clone();

            LStrip.traverse( function ( child ) {
                if ( child instanceof THREE.Mesh ) {
                    child.material = materialLStrip;
                    child.material.side = THREE.FrontSide;
                    //child.material.depthTest = false;
                    child.renderOrder = 10;
                }
            } );
            scene.add( LStrip );
            modelsMinMaxPositions["LStrip"] = getObjectMinMaxPosition(LStrip);

            LStripB.traverse( function ( child ) {
                if ( child instanceof THREE.Mesh ) {
                    child.material = materialLStripB;
                    child.material.side = THREE.BackSide;
                    child.material.depthTest = false;
                    child.renderOrder = 1;
                }
            } );
            scene.add( LStripB );

            },
        xhr => console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' ),
        xhr => console.log( 'An error happened' )
    );*/


    LStrip = LStrip;
    LStrip.material = materialLStrip;
    LStrip.material.side = THREE.FrontSide;
    LStrip.material.depthTest = true;
    LStrip.material.depthWrite = true;
    //LStrip.material.depthFunc = THREE.LessEqualDepth;
    LStrip.renderOrder = params.LStripFRenderOrder;
    scene.add(LStrip);

    LStripB = LStrip.clone();
    LStripB.material = materialLStripB;
    LStripB.material.side = THREE.DoubleSide; //BackSide
    LStripB.material.depthTest = false;
    LStripB.material.depthWrite = true;
    //LStripB.material.depthFunc = THREE.GreaterEqualDepth;
    LStripB.renderOrder = params.LStripBRenderOrder;
    scene.add(LStripB);


    SStrip = SStrip;
    SStrip.material = materialSStrip;
    SStrip.material.side = THREE.FrontSide;
    SStrip.material.depthTest = true;
    SStrip.material.depthWrite = true;
    //SStrip.material.depthFunc = THREE.LessEqualDepth;
    SStrip.renderOrder = params.SStripFRenderOrder;
    scene.add(SStrip);

    SStripB = SStrip.clone();
    SStripB.material = materialSStripB;
    SStripB.material.side = THREE.DoubleSide;
    SStripB.material.depthTest = false;
    SStripB.material.depthWrite = true;
    //SStripB.material.depthFunc = THREE.GreaterEqualDepth;
    SStripB.renderOrder = params.SStripBRenderOrder;
    scene.add(SStripB);


    Pix = Pix;
    Pix.material = materialPix;
    Pix.material.side = THREE.FrontSide;
    Pix.material.depthTest = true;
    Pix.material.depthWrite = true;
    //Pix.material.depthFunc = THREE.LessEqualDepth;
    Pix.renderOrder = params.pixFRenderOrder;
    scene.add(Pix);

    PixB = Pix.clone();
    PixB.material = materialPixB;
    PixB.material.side = THREE.DoubleSide;
    PixB.material.depthTest = false;
    PixB.material.depthWrite = true;
    //PixB.material.depthFunc = THREE.GreaterEqualDepth;
    PixB.renderOrder = params.pixBRenderOrder;
    scene.add(PixB);



    //scene.add(combinedDetectorsObject);



    modelsMinMaxPositions["Pix"] = getObjectMinMaxPosition(Pix);
    modelsMinMaxPositions["SStrip"] = getObjectMinMaxPosition(SStrip);
    modelsMinMaxPositions["LStrip"] = getObjectMinMaxPosition(LStrip);



    //DEBUGGING PURPOSE NORMALS
    //var vnh = new THREE.VertexNormalsHelper( LStrip, 4, 0xff0000, 2 );
    //scene.add( vnh );
    //var fnh = new THREE.FaceNormalsHelper( LStrip, 8, 0x00ff00, 2 );
    //scene.add( fnh );
}



function getObjectMinMaxPosition(object){
    //console.log(object); ///child[0], geometry, attributes
    //console.log(object.children[0].geometry.attributes.position);
    var positionObject;
    if(object.children[0] !== undefined){ //check because if mixed object types
        positionObject = object.children[0].geometry.attributes.position;
    }else{
        positionObject = object.geometry.attributes.position;
    }
    var positions = positionObject.array;
    var count = positionObject.count;
    var itemSize = positionObject.itemSize; // itemSize = 3
    var min = Number.POSITIVE_INFINITY, max = Number.NEGATIVE_INFINITY;
    var current = new THREE.Vector2();

    for(var i = 0; i < count; i++){
        current.x = positions[i*itemSize + 0];
        current.y = positions[i*itemSize + 1];

        if(current.length() > max) max = current.length();
        if(current.length() < min) min = current.length();
    }

    return {min: min, max: max};
}

function copyValue(destination, value, startIndex, count){
    for(let di = startIndex; di < startIndex+count; di++){
        destination[di] = value;
    }
}
function copyArray(destination, source, startIndex, count){
    for(let di = startIndex, si = 0; di < startIndex+count; di++, si++){
        destination[di] = source[si];
    }
}