class XMLHttpRequest_controller{

    constructor(onload_function, progress_function, load_function, error_function, abort_function){
        this.request = new XMLHttpRequest();
        this.constructor(this.request, onload_function, progress_function, load_function, error_function, abort_function);
    }
    constructor(request, onload_function, progress_function, load_function, error_function, abort_function){
        this.request.onload = onload_function;

        this.request.addEventListener("progress", progress_function);
        this.request.addEventListener("load", load_function);
        this.request.addEventListener("error", error_function);
        this.request.addEventListener("abort", abort_function);
    }


    setRequest(request){
        this.request = request;
    }
    getRequest(){
        return this.request;
    }

    initRequest(type, file_path, async){
        this.request.open(type, file_path, async);
        this.request.send();
    }
}