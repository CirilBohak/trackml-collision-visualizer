function submissionFileUpdateProgress(oEvent){
    if (oEvent.lengthComputable) {
        var percentComplete = oEvent.loaded / oEvent.total * 100;
        compareProgressBar.style.width = percentComplete + "%";
    } else {
        // Unable to compute progress information since the total size is unknown
    }
}
function submissionFileTransferComplete(evt) {
    //final update call
    submissionFileUpdateProgress(evt);

    transferComplete(evt);
}
function submissionFileTransferFailed(evt) {
    transferFailed(evt);


    //LOAD COMPLETE
    //remove loading circle
    compareProgressCircle.remove();

    //add folder icon
    compareFolderIcon = document.createElement("DIV");
    compareFolderIcon.classList.add("iconFolder");
    compareProgressHolder.appendChild(compareFolderIcon);

    //modify bar
    compareProgressBar.style.width = 100 + "%";
    compareProgressBar.style.background = params.colorErr;

    //notify
    let message = "Error reading submission data!";
    console.log(message);
    $("#GUIEvents").notify(message, { position:"bottom left", className:"error" });
}
function submissionFileTransferCanceled(evt) {
    transferCanceled(evt);
}



var compareProgressHolder;
var compareProgressBar;
var compareProgressCircle;
function readSubmissionFile(fileName, eventID, compareController){
    compareProgressBar = compareController.progressBar;
    compareProgressCircle = compareController.progressCircle;
    compareProgressHolder = compareController.progressHolder;


    var submissionReq = new XMLHttpRequest();
    submissionReq.onload = function(e) {
        let submissionData;
        var data = submissionReq.response;

        var currentLineIndex = 0;
        var currentLineData;
        var currentLineStartIndex = 0;
        var currentLineLength = 0;

        var selectedEventData = [];

        for(var i = 0; i < data.length; i++){
            currentLineLength++;

            if(data[i] === '\n'){
                currentLineData = data.substr(currentLineStartIndex, currentLineLength);
                if(currentLineIndex === 0 || parseInt(currentLineData.split(',')[0]) === parseInt(eventID)){ //header + corresponding event data
                    selectedEventData.push(currentLineData);
                }

                //end of line
                currentLineStartIndex += currentLineLength;
                currentLineLength = 0;
                currentLineIndex++;
            }
        }

        submissionData = JSON.parse(csvToJSON(selectedEventData.join('\n')));


        //LOAD COMPLETE
        //remove loading circle
        compareProgressCircle.remove();

        //add folder icon
        compareFolderIcon = document.createElement("DIV");
        compareFolderIcon.classList.add("iconFolder");
        compareProgressHolder.appendChild(compareFolderIcon);


        //check if hit size is consistent
        if(submissionData.length === g_Truth.length) {
            //data consistent
            compareProgressBar.style.background = params.colorOK;

            //load submission data
            /*loadSubmissionData(submissionData);

            //init submission gui
            if(!submissionTrackGUInitialized) {
                submissionTrackGUInitialized = true;
                addGUISubmissionTrackList();
            }else{
                updateGUITrackList(submissionTrackList, Object.keys(submissionTrackList), folderSubmissionTracks, guiSubmissionTrackControllerList);
            }*/
        }else{
            //data not consistent
            compareProgressBar.style.background = params.colorErr;
            //popup
            $("#GUIEvents").notify("Submission data size is not consistent with event data size!", { position:"bottom left", className:"error" });
        }

        loadSubmissionData(submissionData);
        addGUISubmissionTrackList();
    };

    submissionReq.addEventListener("progress", submissionFileUpdateProgress);
    submissionReq.addEventListener("load", submissionFileTransferComplete);
    submissionReq.addEventListener("error", submissionFileTransferFailed);
    submissionReq.addEventListener("abort", submissionFileTransferCanceled);

    //submissionReq.open("GET", "./data/csv/sample_submission.csv", true);
    submissionReq.open("GET", fileName, true);
    submissionReq.send();
}
function readSubmissionFile_FR(fileName, eventID, compareController){
    compareProgressBar = compareController.progressBar;
    compareProgressCircle = compareController.progressCircle;
    compareProgressHolder = compareController.progressHolder;


    var fr = new FileReader();
    fr.readAsText(fileName);
    fr.onload = function(){
        let submissionData;
        var data = fr.result;

        var currentLineIndex = 0;
        var currentLineData;
        var currentLineStartIndex = 0;
        var currentLineLength = 0;

        var selectedEventData = [];

        for(var i = 0; i < data.length; i++){
            currentLineLength++;

            if(data[i] === '\n'){
                currentLineData = data.substr(currentLineStartIndex, currentLineLength);
                if(currentLineIndex === 0 || parseInt(currentLineData.split(',')[0]) === parseInt(eventID)){ //header + corresponding event data
                    selectedEventData.push(currentLineData);
                }

                //end of line
                currentLineStartIndex += currentLineLength;
                currentLineLength = 0;
                currentLineIndex++;
            }
        }

        submissionData = JSON.parse(csvToJSON(selectedEventData.join('\n')));


        //LOAD COMPLETE
        //remove loading circle
        compareProgressCircle.remove();

        //add folder icon
        compareFolderIcon = document.createElement("DIV");
        compareFolderIcon.classList.add("iconFolder", "expendable");
        compareProgressHolder.appendChild(compareFolderIcon);


        //check if hit size is consistent
        if(g_Truth !== undefined && submissionData.length === g_Truth.length) {
            //data consistent
            compareProgressBar.style.background = params.colorOK;


        }else{
            //data not consistent
            compareProgressBar.style.background = params.colorWarn;
            //popup
            $("#GUIEvents").notify("Submission data size is not consistent with event data size!", { position:"bottom left", className:"warn" });
        }

        //load submission data
        loadSubmissionData(submissionData);

        //init submission gui
        /*if(!submissionTrackGUInitialized) {
            submissionTrackGUInitialized = true;
            addGUISubmissionTrackList();
        }else{
            updateGUITrackList(submissionTrackList, Object.keys(submissionTrackList), folderSubmissionTracks, guiSubmissionTrackControllerList);
        }*/
        addGUISubmissionTrackList();
    };

    fr.addEventListener("progress", submissionFileUpdateProgress);
    fr.addEventListener("load", submissionFileTransferComplete);
    fr.addEventListener("error", submissionFileTransferFailed);
    fr.addEventListener("abort", submissionFileTransferCanceled);
}

var submittedTracksObject;
var submittedTrackMaterial;
function loadSubmissionData(submissionData){
    //set submission compare flag
    //params.compareSubmissionData = true;


    /*var submittedHitsParticleID = new Float32Array(submissionData.length);
    for(var i = 0; i < submissionData.length; i++){
        //mapping function
        submittedHitsParticleID[i] = mapSubmittedID(submissionData[i].track_id);
    }
    hitsObject.geometry.addAttribute("submitted_hit_particle_id", new THREE.BufferAttribute(submittedHitsParticleID, 1));*/
    //console.log("Submitted particle ID: "); console.log(submittedHitsParticleID);


    //set shader material uniform value
    //if(hitMaterial !== undefined) hitMaterial.uniforms.submissionTesting.value = +params.compareSubmissionData;
    //hitsObject.material.uniforms.submissionTesting.value = +params.compareSubmissionData;


    var submittedTracks = {};
    var submittedTrackID;
    var submittedHitID;
    var attributesInHit;
    for(var i = 0; i < submissionData.length; i++){
        submittedTrackID = submissionData[i].track_id; //for searching purposes
        submittedHitID = submissionData[i].hit_id;
        //attributesInHit = g_Truth[submittedHitID-1];
        attributesInHit = Object.assign({}, g_Truth[i]); //clone object (shallow)
        if(objectIsEmpty(attributesInHit)) continue; //if no mapping found, skip!!! (check needed if submission is bigger than truth)
        attributesInHit.particle_id = submittedTrackID; //rewire to new (submission) ID

        //individual whole tracks
        //if(submittedTrackID === 0) return; //add to tracks if its not noise (noise id = 0)
        if(submittedTracks[submittedTrackID] === undefined) {
            submittedTracks[submittedTrackID] = [];

            //add initial particle
            //submittedTracks[submittedTrackID].push(trackList[]);
        }
        submittedTracks[submittedTrackID].push(attributesInHit);
    }
    submissionTrackList = submittedTracks;


    /******************************************* SUBMITTED TRACKS ******************************************/
        //GEOMETRY + INDICES
    var tracksGeometry = new THREE.BufferGeometry();
    var tracksIndices = [];
    var tracksVertexSize = submissionData.length;

    var trackPairPositions = new Float32Array(tracksVertexSize * 3);
    var trackPairMomentums = new Float32Array(tracksVertexSize * 3);
    var tracksIDs = new Float32Array(tracksVertexSize);
    var i = 0;
    var k = 0;
    for(var key in submittedTracks){
        for(var j = 0; j < submittedTracks[key].length; j++) {
            trackPairPositions[i + 0] = submittedTracks[key][j].tx;
            trackPairPositions[i + 1] = submittedTracks[key][j].ty;
            trackPairPositions[i + 2] = submittedTracks[key][j].tz;

            trackPairMomentums[i + 0] = submittedTracks[key][j].tpx;
            trackPairMomentums[i + 1] = submittedTracks[key][j].tpy;
            trackPairMomentums[i + 2] = submittedTracks[key][j].tpz;

            tracksIDs[k + 0] = key;

            tracksIndices.push(k);

            if(j > 0 && j < submittedTracks[key].length-1){
                tracksIndices.push(k);
            }


            i += 3; k += 1;
        }
    }

    tracksGeometry.setIndex(tracksIndices);
    //console.log("Tracks indices: "); console.log(tracksIndices);

    tracksGeometry.addAttribute("position", new THREE.BufferAttribute(trackPairPositions, 3));
    //console.log("Tracks position: "); console.log(trackPairPositions);

    tracksGeometry.addAttribute("momentum", new THREE.BufferAttribute(trackPairMomentums, 3));
    //console.log("Tracks momentum: "); console.log(trackPairMomentums);

    tracksGeometry.addAttribute("track_id", new THREE.BufferAttribute(tracksIDs, 1));
    //console.log("Tracks ids: "); console.log(tracksIDs);


    //MATERIAL
    submittedTrackMaterial = new THREE.ShaderMaterial({
        vertexShader: document.getElementById("vertexShaderTrack").textContent,
        fragmentShader: document.getElementById("fragmentShaderTrack").textContent,
        uniforms: {
            //selectedTrackID: {value: null}
            selectedTrackVisible: {type: "i", value: +params.visibleSelectedTrack},
            selectedTrackID: {value: params.selectedTrackID},
            numTracks: {value: Object.keys(submittedTracks).length},
            track_size: {value: params.tracksSize},
            track_color: {type: "v3", value: new THREE.Color(params.submittedTracksColor)},
            track_alpha: {value: params.opacityTracks},
            track_color_type: {type: "i", value: params.tracksColorType},
        },
        transparent: true,
        depthTest: false,
        depthWrite: false
    });


    //OBJECT
    submittedTracksObject = new THREE.LineSegments(tracksGeometry, submittedTrackMaterial);



    //ADD TO SCENE
    submittedTracksObject.visible = params.visibleTracks;

    submittedTracksObject.name = "SubmissionTracks";

    scene.remove(scene.getObjectByName("SubmissionTracks"));

    submittedTracksObject.renderOrder = params.submittedTracksRenderOrder;

    scene.add(submittedTracksObject);
}
var submittedIDMap = {};
function mapSubmittedID(submittedTrackID){
    return submittedTrackID;
}
function objectIsEmpty(obj) {
    for(let key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }

    return true;
}