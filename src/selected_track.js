var selectedTrackObject, selectedSubmissionTrackObject;
var selectedTrackMaterial, selectedSubmissionTrackMaterial;
function loadSelectedTrack(trackID, lineWidth){
    var selected_track = trackList[trackID];


    //SELECTED TRACK
    //console.log("Selected track: " ); console.log(selected_track);


    //GEOMETRY
    var selectedTrackGeometry = new THREE.BufferGeometry();

    var position = new Float32Array(selected_track.length * 3);
    for(var i = 0; i < selected_track.length; i++){
        position[i*3 + 0] = selected_track[i].tx;
        position[i*3 + 1] = selected_track[i].ty;
        position[i*3 + 2] = selected_track[i].tz;
    }
    selectedTrackGeometry.addAttribute("position", new THREE.BufferAttribute(position, 3));
    //console.log("Particles attributes: "); console.log(position);
    var momentum = new Float32Array(selected_track.length * 3);
    for(var i = 0; i < selected_track.length; i++){
        momentum[i*3 + 0] = selected_track[i].tpx;
        momentum[i*3 + 1] = selected_track[i].tpy;
        momentum[i*3 + 2] = selected_track[i].tpz;
    }
    selectedTrackGeometry.addAttribute("momentum", new THREE.BufferAttribute(momentum, 3));
    //console.log("Particles attributes: "); console.log(momentum);

    var tracksIDs = new Float32Array(selected_track.length);
    for(var i = 0; i < selected_track.length; i++){
        tracksIDs[i] = trackID; // = selected_track[i].particle_id;
    }
    selectedTrackGeometry.addAttribute("track_id", new THREE.BufferAttribute(tracksIDs, 1));
    //console.log("Tracks ids: "); console.log(tracksIDs);


    //MATERIAL
    var selectedTrackMaterial = new THREE.LineBasicMaterial({
        color: 0xffffff,
        linewidth: lineWidth,
        linecap: 'round', //ignored by WebGLRenderer
        linejoin:  'round' //ignored by WebGLRenderer
    });


    //OBJECT
    selectedTrackObject = new THREE.Line(selectedTrackGeometry, selectedTrackMaterial);



    //ADD TO SCENE
    selectedTrackObject.name = "SelectedTrack";
    scene.remove(scene.getObjectByName("SelectedTrack"));
    selectedTrackObject.renderOrder = params.selectedtrackRenderOrder;
    scene.add(selectedTrackObject);
} //gl_LineWidth
function loadSelectedTrack2(trackID, pointSize){
    var selected_track = trackList[trackID];
    var point_track = [];

    var t = new THREE.Vector3();
    var d, nPoints;
    var v1 = new THREE.Vector3();
    var v2 = new THREE.Vector3();
    for(var i = 0; i < selected_track.length - 1; i++){
        v1.x = selected_track[i].tx;
        v1.y = selected_track[i].ty;
        v1.z = selected_track[i].tz;
        v2.x = selected_track[i + 1].tx;
        v2.y = selected_track[i + 1].ty;
        v2.z = selected_track[i + 1].tz;

        t.subVectors(v2, v1);
        d = t.length();

        nPoints = d/pointSize;


        var pointLocation;
        for(var k = 0; k < nPoints; k+=1/2){ // < N or <= N, both ok
            pointLocation = new THREE.Vector3();
            pointLocation.x = v1.x + ((v2.x - v1.x)/d) * k*pointSize;
            pointLocation.y = v1.y + ((v2.y - v1.y)/d) * k*pointSize;
            pointLocation.z = v1.z + ((v2.z - v1.z)/d) * k*pointSize;

            point_track.push(pointLocation);
        }
    }



    //SELECTED TRACK POINTS
    //console.log("Selected track points: " ); console.log(point_track);


    //GEOMETRY
    var selectedTrackGeometry = new THREE.BufferGeometry();

    var position = new Float32Array(point_track.length * 3);
    for(var i = 0; i < point_track.length; i++){
        position[i*3 + 0] = point_track[i].x;
        position[i*3 + 1] = point_track[i].y;
        position[i*3 + 2] = point_track[i].z;
    }
    selectedTrackGeometry.addAttribute("position", new THREE.BufferAttribute(position, 3));
    //console.log("Particles attributes: "); console.log(position);

    /*var tracksIDs = new Float32Array(point_track.length);
    for(var i = 0; i < point_track.length; i++){
        tracksIDs[i] = trackID; // = selected_track[i].particle_id;
    }
    selectedTrackGeometry.addAttribute("track_id", new THREE.BufferAttribute(tracksIDs, 1));
    //console.log("Tracks ids: "); console.log(tracksIDs);*/


    //MATERIAL
    var selectedTrackMaterial = new THREE.PointsMaterial({
        color: 0xffffff,
        size: pointSize,
        lights: false
    });


    //OBJECT
    selectedTrackObject = new THREE.Points(selectedTrackGeometry, selectedTrackMaterial);



    //ADD TO SCENE
    selectedTrackObject.name = "SelectedTrack";
    scene.remove(scene.getObjectByName("SelectedTrack"));
    selectedTrackObject.renderOrder = params.selectedtrackRenderOrder;
    scene.add(selectedTrackObject);
} //eqdistance points
function loadSelectedTrack3(trackID, lineWidth){
    var selected_track = trackList[trackID];


    //SELECTED TRACK
    //console.log("Selected track: " ); console.log(selected_track);


    //GEOMETRY
    var selectedTrackGeometry = new THREE.BufferGeometry();

    var position = new Float32Array(selected_track.length*2 * 3);
    var normal = new Float32Array(selected_track.length*2 * 3);
    for(var i = 0; i < selected_track.length; i++) {
        position[i * 6 + 0] = selected_track[i].tx;
        position[i * 6 + 1] = selected_track[i].ty;
        position[i * 6 + 2] = selected_track[i].tz;
        position[i * 6 + 3] = selected_track[i].tx;
        position[i * 6 + 4] = selected_track[i].ty;
        position[i * 6 + 5] = selected_track[i].tz;

        normal[i*6 + 0] = -selected_track[i].ty;
        normal[i*6 + 1] = selected_track[i].tx;
        normal[i*6 + 2] = selected_track[i].tz;
        normal[i*6 + 3] = selected_track[i].ty;
        normal[i*6 + 4] = -selected_track[i].tx;
        normal[i*6 + 5] = selected_track[i].tz;
        /*if (i < selected_track.length - 1) {
			normal[i * 6 + 0] = -(selected_track[i + 1].ty - selected_track[i].ty);
			normal[i * 6 + 1] = selected_track[i + 1].tx - selected_track[i].tx;
			normal[i * 6 + 2] = selected_track[i].tz;
			normal[i * 6 + 3] = selected_track[i + 1].ty - selected_track[i].ty;
			normal[i * 6 + 4] = -(selected_track[i + 1].tx - selected_track[i].tx);
			normal[i * 6 + 5] = selected_track[i].tz;
    	}else{
            normal[i * 6 + 0] = -(selected_track[i].ty - selected_track[i - 1].ty);
            normal[i * 6 + 1] = selected_track[i].tx - selected_track[i - 1].tx;
            normal[i * 6 + 2] = 0;
            normal[i * 6 + 3] = selected_track[i].ty - selected_track[i - 1].ty;
            normal[i * 6 + 4] = -(selected_track[i].tx - selected_track[i - 1].tx);
            normal[i * 6 + 5] = 0;
		}*/
    }
    /*for(var i = 3; i < normal.length-1; i+=3) {
        normal[i] = (normal[i - 3] + normal[i + 3])/2;
        normal[i + 1] = (normal[i + 1 - 3] + normal[i + 1 + 3])/2;
        normal[i + 2] = (normal[i + 2 - 3] + normal[i + 2 + 3])/2;
    }*/
    selectedTrackGeometry.addAttribute("position", new THREE.BufferAttribute(position, 3));
    //console.log("Particles attributes: "); console.log(position);
    selectedTrackGeometry.addAttribute("normal", new THREE.BufferAttribute(normal, 3));
    //console.log("Particles attributes: "); console.log(normal);



    //MATERIAL
    var selectedTrackMaterial = new THREE.ShaderMaterial({
        vertexShader: document.getElementById("vertexShaderSelectedTrack3").textContent,
        fragmentShader: document.getElementById("fragmentShaderSelectedTrack3").textContent,
        uniforms: {
            line_width: {value: lineWidth},
            track_color: {type: "v3", value: new THREE.Vector3(1.0, 1.0, 1.0)},
            track_alpha: {value: 1.0},

            minMaxBeamPipe: {type: "v2", value: new THREE.Vector2(modelsMinMaxPositions["BeamPipe"].min, modelsMinMaxPositions["BeamPipe"].max)},
            minMaxPix: {type: "v2", value: new THREE.Vector2(modelsMinMaxPositions["Pix"].min, modelsMinMaxPositions["Pix"].max)},
            minMaxPST: {type: "v2", value: new THREE.Vector2(modelsMinMaxPositions["PST"].min, modelsMinMaxPositions["PST"].max)},
            minMaxSStrip: {type: "v2", value: new THREE.Vector2(modelsMinMaxPositions["SStrip"].min, modelsMinMaxPositions["SStrip"].max)},
            minMaxLStrip: {type: "v2", value: new THREE.Vector2(modelsMinMaxPositions["LStrip"].min, modelsMinMaxPositions["LStrip"].max)},

            colorBeamPipe: {type: "v3", value: new THREE.Color(params.colorBeamPipe).add(new THREE.Color(params.ecolorBeamPipe))},
            colorPix: {type: "v3", value: new THREE.Color(params.colorPix).add(new THREE.Color(params.ecolorPix))},
            colorPST: {type: "v3", value: new THREE.Color(params.colorPST).add(new THREE.Color(params.ecolorPST))},
            colorSStrip: {type: "v3", value: new THREE.Color(params.colorSStrip).add(new THREE.Color(params.ecolorSStrip))},
            colorLStrip: {type: "v3", value: new THREE.Color(params.colorLStrip).add(new THREE.Color(params.ecolorLStrip))}
        },
        side: THREE.DoubleSide,
        transparent: true,
        depthTest: false,
        depthWrite: false,
        wireframe: false
    });


    //OBJECT
    selectedTrackObject = new THREE.Mesh(selectedTrackGeometry, selectedTrackMaterial);
    selectedTrackObject.drawMode = THREE.TriangleStripDrawMode; //mesh.drawMode = THREE.TrianglesDrawMode; // default



    //ADD TO SCENE
    selectedTrackObject.name = "SelectedTrack";
    scene.remove(scene.getObjectByName("SelectedTrack"));
    selectedTrackObject.renderOrder = params.selectedtrackRenderOrder;
    scene.add(selectedTrackObject);
} //simple strip
function loadSelectedTrack31(trackList, trackID, sceneObjectName){
    var selected_track = trackList[trackID];
    if(selected_track.length === 1) {
        loadSelectedTrackAsHit(selected_track, sceneObjectName);

        return;
    }


    //SELECTED TRACK
    //console.log("Selected track: " ); console.log(selected_track);


    //GEOMETRY
    var selectedTrackGeometry = new THREE.BufferGeometry();

    var position = new Float32Array(selected_track.length * 2 * 3);
    var prevPosition = new Float32Array(selected_track.length * 2 * 3);
    var nextPosition = new Float32Array(selected_track.length * 2 * 3);
    var normalDirection = new Float32Array(selected_track.length * 2);
    var momentum = new Float32Array(selected_track.length * 2 * 3);
    let momentumVec = new THREE.Vector3();
    for(var i = 0; i < selected_track.length; i++) {
        position[i * 6 + 0] = selected_track[i].tx;
        position[i * 6 + 1] = selected_track[i].ty;
        position[i * 6 + 2] = selected_track[i].tz;

        position[i * 6 + 3] = selected_track[i].tx;
        position[i * 6 + 4] = selected_track[i].ty;
        position[i * 6 + 5] = selected_track[i].tz;


        if (i > 0) {
            prevPosition[i * 6 + 0] = selected_track[i - 1].tx;
            prevPosition[i * 6 + 1] = selected_track[i - 1].ty;
            prevPosition[i * 6 + 2] = selected_track[i - 1].tz;

            prevPosition[i * 6 + 3] = selected_track[i - 1].tx;
            prevPosition[i * 6 + 4] = selected_track[i - 1].ty;
            prevPosition[i * 6 + 5] = selected_track[i - 1].tz;
        }else if(i == 0) {
            prevPosition[i * 6 + 0] = selected_track[i].tx;
            prevPosition[i * 6 + 1] = selected_track[i].ty;
            prevPosition[i * 6 + 2] = selected_track[i].tz;

            prevPosition[i * 6 + 3] = selected_track[i].tx;
            prevPosition[i * 6 + 4] = selected_track[i].ty;
            prevPosition[i * 6 + 5] = selected_track[i].tz;
        }


        if(i < selected_track.length-1) {
            nextPosition[i * 6 + 0] = selected_track[i + 1].tx;
            nextPosition[i * 6 + 1] = selected_track[i + 1].ty;
            nextPosition[i * 6 + 2] = selected_track[i + 1].tz;

            nextPosition[i * 6 + 3] = selected_track[i + 1].tx;
            nextPosition[i * 6 + 4] = selected_track[i + 1].ty;
            nextPosition[i * 6 + 5] = selected_track[i + 1].tz;
        }else if(i == selected_track.length-1){
            nextPosition[i * 6 + 0] = selected_track[i].tx;
            nextPosition[i * 6 + 1] = selected_track[i].ty;
            nextPosition[i * 6 + 2] = selected_track[i].tz;

            nextPosition[i * 6 + 3] = selected_track[i].tx;
            nextPosition[i * 6 + 4] = selected_track[i].ty;
            nextPosition[i * 6 + 5] = selected_track[i].tz;
        }


        normalDirection[i * 2 + 0] = -1;
        normalDirection[i * 2 + 1] = 1;


        momentumVec.x = selected_track[i].tpx;
        momentumVec.y = selected_track[i].tpy;
        momentumVec.z = selected_track[i].tpz;
        momentumVec.normalize();
        momentumVec.multiplyScalar(0.5);
        momentumVec.addScalar(0.5);

        momentum[i * 6 + 0] = momentumVec.x;
        momentum[i * 6 + 1] = momentumVec.y;
        momentum[i * 6 + 2] = momentumVec.z;

        momentum[i * 6 + 3] = momentumVec.x;
        momentum[i * 6 + 4] = momentumVec.y;
        momentum[i * 6 + 5] = momentumVec.z;
    }


    selectedTrackGeometry.addAttribute("position", new THREE.BufferAttribute(position, 3));
    //console.log("Particles position: "); console.log(position);
    selectedTrackGeometry.addAttribute("prevPosition", new THREE.BufferAttribute(prevPosition, 3));
    //console.log("Particles prevPosition: "); console.log(prevPosition);
    selectedTrackGeometry.addAttribute("nextPosition", new THREE.BufferAttribute(nextPosition, 3));
    //console.log("Particles nextPosition: "); console.log(nextPosition);
    selectedTrackGeometry.addAttribute("normalDirection", new THREE.BufferAttribute(normalDirection, 1));
    //console.log("Particles nextPosition: "); console.log(nextPosition);
    selectedTrackGeometry.addAttribute("momentum", new THREE.BufferAttribute(momentum, 3));
    //console.log("Particles momentum: "); console.log(momentum);



    //MATERIAL
    var selectedTrackMaterial = new THREE.ShaderMaterial({
        vertexShader: document.getElementById("vertexShaderSelectedTrack31").textContent,
        fragmentShader: document.getElementById("fragmentShaderSelectedTrack31").textContent,
        uniforms: {
            line_width: {value: params.widthSelectedTrack},
            track_color: {type: "v3", value: new THREE.Color(params.selectedTrackColor)},
            track_alpha: {value: params.opacitySelectedTrack},
            track_color_type: {type: "i", value: params.selectedTrackColorType},
            aspect: {value: window.innerWidth/window.innerHeight},

            minMaxBeamPipe: {type: "v2", value: new THREE.Vector2(modelsMinMaxPositions["BeamPipe"].min, modelsMinMaxPositions["BeamPipe"].max)},
            minMaxPix: {type: "v2", value: new THREE.Vector2(modelsMinMaxPositions["Pix"].min, modelsMinMaxPositions["Pix"].max)},
            minMaxPST: {type: "v2", value: new THREE.Vector2(modelsMinMaxPositions["PST"].min, modelsMinMaxPositions["PST"].max)},
            minMaxSStrip: {type: "v2", value: new THREE.Vector2(modelsMinMaxPositions["SStrip"].min, modelsMinMaxPositions["SStrip"].max)},
            minMaxLStrip: {type: "v2", value: new THREE.Vector2(modelsMinMaxPositions["LStrip"].min, modelsMinMaxPositions["LStrip"].max)},

            colorBeamPipe: {type: "v3", value: new THREE.Color(params.colorBeamPipe).add(new THREE.Color(params.ecolorBeamPipe))},
            colorPix: {type: "v3", value: new THREE.Color(params.colorPix).add(new THREE.Color(params.ecolorPix))},
            colorPST: {type: "v3", value: new THREE.Color(params.colorPST).add(new THREE.Color(params.ecolorPST))},
            colorSStrip: {type: "v3", value: new THREE.Color(params.colorSStrip).add(new THREE.Color(params.ecolorSStrip))},
            colorLStrip: {type: "v3", value: new THREE.Color(params.colorLStrip).add(new THREE.Color(params.ecolorLStrip))}
        },
        side: THREE.DoubleSide,
        transparent: true,
        depthTest: false,
        depthWrite: false,
        wireframe: false
    });


    //OBJECT
    var selectedTrackObject = new THREE.Mesh(selectedTrackGeometry, selectedTrackMaterial);
    selectedTrackObject.drawMode = THREE.TriangleStripDrawMode; //mesh.drawMode = THREE.TrianglesDrawMode; // default



    //ADD TO SCENE
    selectedTrackObject.visible = params.visibleSelectedTrack;
    selectedTrackObject.name = sceneObjectName;
    scene.remove(scene.getObjectByName(sceneObjectName));
    selectedTrackObject.renderOrder = params.selectedtrackRenderOrder;
    scene.add(selectedTrackObject);



    //REWIRE
    if(sceneObjectName === "SelectedSubmissionTrack"){
        selectedTrackMaterial.uniforms.track_color.value = new THREE.Color(params.selectedSubmissionTrackColor);
        window.selectedSubmissionTrackMaterial = selectedTrackMaterial;
        window.selectedSubmissionTrackObject = selectedTrackObject;
    }else if(sceneObjectName === "SelectedTrack"){
        window.selectedTrackMaterial = selectedTrackMaterial;
        window.selectedTrackObject = selectedTrackObject;
    }
} //NCD
function loadSelectedTrack32(trackID, lineWidth){
    var selected_track = trackList[trackID];


    //SELECTED TRACK
    //console.log("Selected track: " ); console.log(selected_track);


    //GEOMETRY
    var selectedTrackGeometry = new THREE.Geometry();

    for(var i = 0; i < selected_track.length; i++) {
        selectedTrackGeometry.vertices.push( new THREE.Vector3(selected_track[i].tx, selected_track[i].ty, selected_track[i].tz) );
    }
    var line = new MeshLine();
    //line.setGeometry( selectedTrackGeometry );
    //line.setGeometry( selectedTrackGeometry, function( p ) { return 2; } ); // makes width 2 * lineWidth
    //line.setGeometry( selectedTrackGeometry, function( p ) { return 1 - p; } ); // makes width taper
    //line.setGeometry( selectedTrackGeometry, function( p ) { return 2 + Math.sin( 50 * p ); } ); // makes width sinusoidal
    line.setGeometry( selectedTrackGeometry, function( p ) { return 1; } );


    //MATERIAL
    var selectedTrackMaterial = new MeshLineMaterial({
        lineWidth: lineWidth
    });


    //OBJECT
    selectedTrackObject = new THREE.Mesh(line.geometry, selectedTrackMaterial);



    //ADD TO SCENE
    selectedTrackObject.name = "SelectedTrack";
    scene.remove(scene.getObjectByName("SelectedTrack"));
    selectedTrackObject.renderOrder = params.selectedtrackRenderOrder;
    scene.add(selectedTrackObject);
} //MeshLine library

function loadSelectedTrackAsHit(selected_track, sceneObjectName){
    //SELECTED TRACK
    //console.log("Selected track: " ); console.log(selected_track);


    //GEOMETRY
    var selectedTrackGeometry = new THREE.BufferGeometry();

    var position = new Float32Array(selected_track.length * 3);
    var momentum = new Float32Array(selected_track.length * 3);

    for(var i = 0; i < selected_track.length; i++) {
        position[i * 3 + 0] = selected_track[i].tx;
        position[i * 3 + 1] = selected_track[i].ty;
        position[i * 3 + 2] = selected_track[i].tz;

        momentum[i * 3 + 0] = selected_track[i].tpx;
        momentum[i * 3 + 1] = selected_track[i].tpy;
        momentum[i * 3 + 2] = selected_track[i].tpz;
    }

    selectedTrackGeometry.addAttribute("position", new THREE.BufferAttribute(position, 3));
    //console.log("Particles position: "); console.log(position);
    selectedTrackGeometry.addAttribute("momentum", new THREE.BufferAttribute(momentum, 3));
    //console.log("Particles momentum: "); console.log(momentum);


    //MATERIAL
    var selectedTrackMaterial = new THREE.ShaderMaterial({
        vertexShader: document.getElementById("vertexShaderSelectedTrackAsHit").textContent,
        fragmentShader: document.getElementById("fragmentShaderSelectedTrackAsHit").textContent,
        uniforms: {
            line_width: {value: params.widthSelectedTrack},
            track_color: {type: "v3", value: new THREE.Color(params.selectedTrackColor)},
            track_alpha: {value: params.opacitySelectedTrack},
            track_color_type: {type: "i", value: params.selectedTrackColorType},
            //aspect: {value: window.innerWidth/window.innerHeight},

            minMaxBeamPipe: {type: "v2", value: new THREE.Vector2(modelsMinMaxPositions["BeamPipe"].min, modelsMinMaxPositions["BeamPipe"].max)},
            minMaxPix: {type: "v2", value: new THREE.Vector2(modelsMinMaxPositions["Pix"].min, modelsMinMaxPositions["Pix"].max)},
            minMaxPST: {type: "v2", value: new THREE.Vector2(modelsMinMaxPositions["PST"].min, modelsMinMaxPositions["PST"].max)},
            minMaxSStrip: {type: "v2", value: new THREE.Vector2(modelsMinMaxPositions["SStrip"].min, modelsMinMaxPositions["SStrip"].max)},
            minMaxLStrip: {type: "v2", value: new THREE.Vector2(modelsMinMaxPositions["LStrip"].min, modelsMinMaxPositions["LStrip"].max)},

            colorBeamPipe: {type: "v3", value: new THREE.Color(params.colorBeamPipe).add(new THREE.Color(params.ecolorBeamPipe))},
            colorPix: {type: "v3", value: new THREE.Color(params.colorPix).add(new THREE.Color(params.ecolorPix))},
            colorPST: {type: "v3", value: new THREE.Color(params.colorPST).add(new THREE.Color(params.ecolorPST))},
            colorSStrip: {type: "v3", value: new THREE.Color(params.colorSStrip).add(new THREE.Color(params.ecolorSStrip))},
            colorLStrip: {type: "v3", value: new THREE.Color(params.colorLStrip).add(new THREE.Color(params.ecolorLStrip))}
        },
        side: THREE.DoubleSide,
        transparent: true,
        depthTest: false,
        depthWrite: false,
        wireframe: false
    });


    //OBJECT
    var selectedTrackObject = new THREE.Points(selectedTrackGeometry, selectedTrackMaterial);
    //selectedTrackObject.drawMode = THREE.TriangleStripDrawMode; //mesh.drawMode = THREE.TrianglesDrawMode; // default



    //ADD TO SCENE
    selectedTrackObject.visible = params.visibleSelectedTrack;
    selectedTrackObject.name = sceneObjectName;
    scene.remove(scene.getObjectByName(sceneObjectName));
    selectedTrackObject.renderOrder = params.selectedtrackRenderOrder;
    scene.add(selectedTrackObject);



    //REWIRE
    if(sceneObjectName === "SelectedSubmissionTrack"){
        selectedTrackMaterial.uniforms.track_color.value = new THREE.Color(params.selectedSubmissionTrackColor);
        window.selectedSubmissionTrackMaterial = selectedTrackMaterial;
        window.selectedSubmissionTrackObject = selectedTrackObject;
    }else if(sceneObjectName === "SelectedTrack"){
        window.selectedTrackMaterial = selectedTrackMaterial;
        window.selectedTrackObject = selectedTrackObject;
    }
}