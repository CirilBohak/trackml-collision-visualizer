let customEventsFilesList = {};


let customEventLoader = {
    clearCustomEventFilesList: function(){
        customEventsFilesList = {};
    },

    readCustomEventFile_FR: function(files, uploadCustomEventLI) {
        let eventID;
        let matchID;
        let matchType;
        let eventFileType;
        this.clearCustomEventFilesList();

        for (let i = 0; i < files.length; i++) {
            //cez vsak file
            //console.log(files[i]);


            //sparsi cifro
            //matchID = files[i].name.match("event[0-9]+");
            matchID = files[i].name.match("event[0-9]{9}");
            //matchType = files[i].name.match("-[a-zA-Z]+\.csv")[0];
            matchType = files[i].name.match("particles\\.|hits\\.|cells\\.|truth\\.");

            if(matchID === null || matchType === null){

                //CLEAN
                //clear previous load progress indicator
                clearListItemProgressIndicator(uploadCustomEventLI, "loaderHolder", "expendable");


                //ADD
                //background + icon
                appendListItemBackground(uploadCustomEventLI, "backgroundHolder", "colorBar", "iconFolder", "expendable", params.colorWarn);


                //notify
                let message = "Invalid name: " + files[i].name;
                //console
                console.log(message);
                //popup
                $("#GUIEvents").notify(message, { position:"bottom left", className:"warn" });


                continue;
            }
            matchID = matchID[0];
            eventID = matchID.substring(5, matchID.length);

            matchType = matchType[0];
            eventFileType = matchType.substring(0, matchType.length-1)


            //na cifro pripni pripadajoce datoteke
            /*if(customEventsFilesList.hasOwnProperty(eventID)){
                customEventsFilesList[eventID].push(files[i]);

            }else{
                customEventsFilesList[eventID] = [files[i]];
            }*/
            if(!customEventsFilesList.hasOwnProperty(eventID)) customEventsFilesList[eventID] = {};

            switch(eventFileType) {
                case "particles":
                    customEventsFilesList[eventID].particles = files[i];
                    break;
                case "hits":
                    customEventsFilesList[eventID].hits = files[i];
                    break;
                case "cells":
                    //customEventsFilesList[eventID].cells = files[i];
                    break;
                case "truth":
                    customEventsFilesList[eventID].truth = files[i];
                    break;
                default:
                    //notify
                    let message = "Event " + eventID + " file name mismatch: " + eventFileType;
                    //console
                    console.log(message);
                    //popup
                    $("#GUIEvents").notify(message, { position:"bottom left", className:"warn" });
            }

        }

        //console.log(customEventsFilesList);


        //CLEAN
        //clear previous load progress indicator
        clearListItemProgressIndicator(uploadCustomEventLI, "loaderHolder", "expendable");


        //ADD
        //icon
        //appendListItemIcon(uploadCustomEventLI, "iconHolder", "iconFolder", "expendable");
        //background + icon
        appendListItemProgressIndicator(uploadCustomEventLI, "backgroundHolder", "colorBar", "iconFolder", "expendable", params.colorOK);
    }
};