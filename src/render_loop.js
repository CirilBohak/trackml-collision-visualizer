var currTime;
var prevTime;
var deltaTime;

var focusCamera = false;
var cameraFocusPosition = new THREE.Vector3();
var cameraTargetFocusPosition = new THREE.Vector3();


var render = function () {
    requestAnimationFrame( render );

    currTime = performance.now();
    deltaTime = ( currTime - prevTime ) / 1000;
    prevTime = currTime;


    //RUN TEST
    //console.log("FPS:" + 1/deltaTime + ", " + "Frame Time: " + deltaTime);
    //stat.test(deltaTime);


    //CAMERA ANIMATION
    if(focusCamera) {
        //orbit.enabled = false;
        cameraFocus(cameraFocusPosition, cameraTargetFocusPosition);
    }

    renderer.render( scene, camera );
};



var intermediatePosition = new THREE.Vector3();
var cameraTarget = new THREE.Vector3();
var intermediateTarget = new THREE.Vector3();
var deltaPosition;
var deltaTarget;
var currentCameraTargetPosition = new THREE.Vector3(0, 0, 0);
function cameraFocus(cameraPosition, targetPosition){
    //if(!cameraAnimating)
    /*if(cancelCam){
        cancelCam == false;
        return;
    }*/

    //intermediatePosition.lerpVectors(camera.position, new THREE.Vector3(cameraPosition.x, cameraPosition.y, cameraPosition.z + 1024), 1.28 * deltaTime); //1024 additional offset //0.05 brez delta time
    intermediatePosition.lerpVectors(camera.position, cameraPosition, 1.28 * deltaTime); //tocka
    camera.position.set(intermediatePosition.x, intermediatePosition.y, intermediatePosition.z); //tocka
    //camera.position.z += 1024;

    //cameraTarget = camera.getWorldDirection(cameraTarget); //vector
    //console.log(camera.getWorldDirection(cameraTarget).length()); ///vektor; vedno je dolzine 1 - radij krogle!!!
    intermediateTarget.lerpVectors(currentCameraTargetPosition, targetPosition, 1.08 * deltaTime);//VECTOR
    currentCameraTargetPosition.x = intermediateTarget.x;
    currentCameraTargetPosition.y = intermediateTarget.y;
    currentCameraTargetPosition.z = intermediateTarget.z;

    //camera.lookAt(currentCameraTargetPosition.x, currentCameraTargetPosition.y, currentCameraTargetPosition.z); //orbit control overrida to//tocka
    orbit.target.set(intermediateTarget.x, intermediateTarget.y, intermediateTarget.z); //tocka (Transform to point)


    orbit.update();


    //camera.updateProjectionMatrix();
    //var delta = Math.abs(camera.position.x - cameraPosition.x);
    deltaPosition = Math.abs(camera.position.length() - cameraPosition.length()); // Vector3.length() return Euclidean distance, Camera has offset  - OK
    deltaTarget = Math.abs(currentCameraTargetPosition.length() - targetPosition.length());
    if(deltaPosition < 0.2 && deltaTarget < 0.1){
        focusCamera = false;
        //orbit.enabled = true;
    }
}