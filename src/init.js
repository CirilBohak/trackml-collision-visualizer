var scene = new THREE.Scene();
var camera, renderer, orbit, lights = [], lightsObject;

function init() {
    let canvasWidth = window.innerWidth;
    let canvasHeight = window.innerHeight;
    //canvasWidth = 854;
    //canvasHeight = 480;
    //canvasWidth = 1024;
    //canvasHeight = 576;
    //canvasWidth = 1280;
    //canvasHeight = 720;
    //canvasWidth = 1366;
    //canvasHeight = 768;
    //canvasWidth = 1600;
    //canvasHeight = 900;
    //canvasWidth = 1920;
    //canvasHeight = 1080;
    //console.log("width: " + canvasWidth);
    //console.log("height: " + canvasHeight);


    camera = new THREE.PerspectiveCamera( 75, canvasWidth / canvasHeight, 0.1, 100000 );
    //set initial camera parameters
    //camera.position.z = 5000;
    camera.position.x = -1820;
    camera.position.y = 1020;
    camera.position.z = 3820;
    camera.up.x = 0;
    camera.up.y = 1;
    camera.up.z = 0;

    renderer = new THREE.WebGLRenderer( { antialias: false } );
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( canvasWidth, canvasHeight );
    renderer.setClearColor( 0x000000, 1 );
    document.body.appendChild( renderer.domElement );

    orbit = new THREE.OrbitControls( camera, renderer.domElement );
    //stop camera animation on mouse focus
    renderer.domElement.addEventListener("mousedown", function(){
        focusCamera = false;
    }, false);
    renderer.domElement.addEventListener("wheel", function(){
        focusCamera = false;
    }, false);
    //
    // orbit.enableZoom = false;
    //renderer.sortObjects = false; //must be true for manual order
    renderer.localClippingEnabled = true; //enable local clipping planes; set in material for certain object
    //renderer.clippingPlanes = [plane]; ///global clipping plane

    lights = [];
    lightsObject = new THREE.Object3D();

    for (var i=0; i<6; i++)
        lights.push(new THREE.PointLight( 0xffffff, 1, 0 ));

    lights[ 0 ].position.set(10000, 0, 0);
    lights[ 1 ].position.set(-10000, 0, 0);
    lights[ 2 ].position.set(0, 10000, 0);
    lights[ 3 ].position.set(0, -10000, 0);
    lights[ 4 ].position.set(0, 0, 10000);
    lights[ 5 ].position.set(0, 0, -10000);

    lights.forEach(light => lightsObject.add(light));
    scene.add(lightsObject);


    window.addEventListener( 'resize', function () {
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();
        renderer.setSize( window.innerWidth, window.innerHeight );
    }, false );


    render();
}